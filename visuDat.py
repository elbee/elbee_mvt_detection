import matplotlib
matplotlib.use('TKAgg')

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np

yawA = []
rollA = []
pitchA = []
countA = []
fname = "/Users/vcanuel/data.raw"
f = open(fname, 'r')

fig1 = plt.figure()
aPitch = plt.axes(xlim=(0, 100), ylim=(-50, 360))
pPitch, = aPitch.plot([], [], 'r.-')
aPitch.grid()

aRoll = plt.axes(xlim=(0, 100), ylim=(-50, 360))
pRoll, = aRoll.plot([], [], 'b.-')

aYaw = plt.axes(xlim=(0, 100), ylim=(-50, 360))
pYaw, = aYaw.plot([], [], 'g.-')

plt.xlabel('TIME')
plt.title('MOVEMENT ANALYSYS')


# initialization function: plot the background of each frame
def init():
    pPitch.set_data([], [])
    pYaw.set_data([], [])
    pRoll.set_data([], [])
    return pPitch,pYaw, pRoll

def update_line(num):
	curLine = f.readline()
	if curLine != '':
		result = curLine.split( ) #Split by ' ' char 
		yawA.append(result[0])
		rollA.append(result[1])
		pitchA.append(result[2])
		countA.append(num)
		xmin, xmax = aPitch.get_xlim()
		if num >= xmax and len(countA) > 20:
			yawA.pop(20);
			rollA.pop(20);
			pitchA.pop(20);
			countA.pop(20);
			aPitch.set_xlim(xmin+20, xmax+20)
			aPitch.figure.canvas.draw()
			aRoll.set_xlim(xmin+20, xmax+20)
			aRoll.figure.canvas.draw()
			aYaw.set_xlim(xmin+20, xmax+20)
			aYaw.figure.canvas.draw()
		pPitch.set_data(countA, pitchA)
		pRoll.set_data(countA, rollA)
		pYaw.set_data(countA, yawA)
	return pPitch, pRoll, pYaw

fig_animation = animation.FuncAnimation(fig1, update_line, init_func=init, interval=50, blit=True)
plt.show()
f.close()


		
