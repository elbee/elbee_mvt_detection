myfile=fopen('gonuts.txt','r');
sizer=[12 66]
q=fscanf(myfile,'%6d %6d %6d %6d %6d %6d %6d %6d %6d %6d %6d %6d \r\n',sizer)
fclose(myfile);


[y p r]=quat2angle(q(1:4,:)');
y=y*180/pi;
p=p*180/pi;
r=r*180/pi;

scrsz = get(0,'ScreenSize')
figure('Position',[1 2*scrsz(4)/3 scrsz(3) scrsz(4)/3])
ax1 = subplot(3,1,1); % top subplot
plot(ax1,q(11,:)/1000,q(5,:)/16)
hold on
plot(ax1,q(11,:)/1000,mod(-y,360),'r')
ylabel('yaw/heading (deg)')

ax2 = subplot(3,1,2); % bottom  left subplot
plot(ax2,q(11,:)/1000,q(6,:)/16)
hold on
plot(ax2,q(11,:)/1000,-p,'r')
ylabel('pitch (deg)')

ax3 = subplot(3,1,3); % bottom rightsubplot
plot(ax3,q(11,:)/1000,q(7,:)/16)
hold on
plot(ax3,q(11,:)/1000,-r,'r')
xlabel('time (s)')
ylabel('roll (deg)')
