clear

f1='avant90.dat';
f2='arriere90.dat';
f3='rotdroite90.dat';
f4='rotgauche90.dat';
f5='droit90.dat';
f6='gauche90.dat';
myfile=fopen(f5,'r');
for i=1:1
    q=fscanf(myfile,'%d',8 );
end
ref=q(1:4,1)';
ref=quaternion(ref);
ref=ref.normalize;
ra=real(ref);
rv=vector(ref);
rollref=atan2(2*(ra*rv(1)+rv(2)*rv(3)),1-2*(rv(1)^2+rv(2)^2))
roll2=q(7)/16
pitchref=asin(2*(ra*rv(2)-rv(3)*rv(1)))
pitch2=q(6)/16
headref=atan2(2*(ra*rv(3)+rv(1)*rv(2)),1-2*(rv(2)^2+rv(3)^2))
head2=q(5)/16
angles=EulerAngles(ref,'123');

for i=1:59
    q=fscanf(myfile,'%d',8 );
    quat=q(1:4)';
    quat=quaternion(quat);
    quat=quat.normalize;
    qa=real(quat);
    qv=vector(quat);
    rollnew=atan2(2*qa*qv(1)+qv(2)*qv(3),1-2*(qv(1)^2+qv(2)^2))*180/pi;
    roll(i)=(rollnew-rollref);
    rollbis(i)=(q(7)/16-roll2);
    pitchnew=asin(2*(qa*qv(2)-qv(3)*qv(1)))*180/pi;
    pitch(i)=(pitchnew-pitchref); 
    pitchbis(i)=(q(6)/16-pitch2);
    headnew=atan2(2*(qa*qv(3)+qv(1)*qv(2)),1-2*(qv(2)^2+qv(3)^2));
    head(i)=(headnew-headref)*180/pi;
    headbis(i)=(q(5)/16-head2);
end
    
scrsz = get(0,'ScreenSize');
figure('Position',[0 scrsz(4)/2 scrsz(3)/3 scrsz(4)/2.5]);
plot(head)
title('Heading','FontSize',20);

figure('Position',[scrsz(3)/3 scrsz(4)/2 scrsz(3)/3 scrsz(4)/2.5]);
plot(pitch)
title('Pitch','FontSize',20);

figure('Position',[2*scrsz(3)/3 scrsz(4)/2 scrsz(3)/3 scrsz(4)/2.5]);
plot(roll)
title('Roll','FontSize',20);

figure('Position',[0 0*scrsz(4)/6 scrsz(3)/3 scrsz(4)/2.5]);
plot(headbis)
title('Heading','FontSize',20);

figure('Position',[scrsz(3)/3 0*scrsz(4) scrsz(3)/3 scrsz(4)/2.5]);
plot(pitchbis)
title('Pitch','FontSize',20);

figure('Position',[2*scrsz(3)/3 0*scrsz(4)/2.5 scrsz(3)/3 scrsz(4)/2.5]);
plot(rollbis)
title('Roll','FontSize',20);

fclose(myfile);    
