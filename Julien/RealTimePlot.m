clear %clear variables
clc %clear command window (clear screen)

%User Defined Properties 
serialPort = 'COM3';            % define COM port #
baudRate = 9600;                
plotTitle = 'Heading data Log'; % plot title
xLabel = 'Time (s)';            % x-axis label
yLabel = 'Heading';             % y-axis label
plotGrid = 'on';                % 'off' to turn off grid
min = 0;                        % set y-min for heading
max = 360;                      % set y-max
min2= -90;                      % set y-min for pitch
max2= 90;                       % set y-min 
min3= -180;                     % set y-min for roll
max3= 180;                      % set y-max 
scrollWidth = 10;               % display period in plot, plot entire yaw log if <= 0
delay = .01;                    % make sure sample faster than resolution
freq = 10;                      %frequency of new yaw from IMU per sec
critTimeMin = 0.5;              %min time in which the movement must be done
critTimeMax = 2;                %max time in which the movement must be done
countMax=round(critTimeMax*freq);%maximum size of the frame used to check for specific head motion 
eps = 3;                        %difference of angle tolerated with respect to the initial position
critAngle  = 20;                %critical angle (rotate left or right) from which an action is triggered
critAngle2 = 15;                %critical angle (up or down) from which an action is triggered
critAngle3 = 15;                %critical angle (tilt left or right) from which an action is triggered
noseoffset = 45;                %offset of the direction of the nose with respect to the x-axis of the IMU 

time = 0;
yaw = 0;
pitch = 0;
roll = 0;
count = 0;

%Set up Plot
%Heading
scrsz = get(0,'ScreenSize');
%figure('Position',[0 40 scrsz(3)/3 scrsz(4)/3]);
figure('Position',[0 2*scrsz(4)/3 2*scrsz(3)/3 scrsz(4)/4.3]);
plotGraph = plot(time,yaw);%,time,pitch,time,roll);
title(plotTitle,'FontSize',20);
xlabel(xLabel,'FontSize',10);
ylabel(yLabel,'FontSize',10);
%axis([0 10 min max]);

grid(plotGrid);
%Pitch
% figure('Position',[scrsz(3)/3 40 scrsz(3)/3 scrsz(4)/3]);
figure('Position',[0 scrsz(4)/3 2*scrsz(3)/3 scrsz(4)/4.3]);
plotGraph2 = plot(time,pitch);%,time,pitch,time,roll);
title('Pitch data Log','FontSize',20);
xlabel(xLabel,'FontSize',10);
ylabel('Pitch','FontSize',10);
%axis([0 10 min2 max2]);

grid(plotGrid);
%Roll
%figure('Position',[2*scrsz(3)/3 40 scrsz(3)/3 scrsz(4)/3]);
figure('Position',[0 0 2*scrsz(3)/3 scrsz(4)/4.3]);
plotGraph3 = plot(time,roll);%,time,pitch,time,roll);
title('Roll data Log','FontSize',20);
xlabel(xLabel,'FontSize',10);
ylabel('Roll','FontSize',10);
%axis([0 10 min3 max3]);
grid(plotGrid);

%Open Serial COM Port

tic %Start clock to measure performance

lastdetection=0; %initializing when is the last time we detected a recognized motion

disp('Press enter when ready for the reference');
pause;
s = serial(serialPort, 'BaudRate',baudRate);
disp('Close Plot to End Session');
fopen(s);
dat=fscanf(s,'%d');%Read yaw from Serial as Integer
while (~isnumeric(dat)| isempty(dat)| length(dat)~=8)
    dat=fscanf(s,'%d');%Read yaw from Serial as Integer
end
ref=[dat(5)/16 dat(6)/16 dat(7)/16];
refquat=[dat(1) dat(2) dat(3) dat(4)]
[bla ble blu]=quat2angle(refquat);
test0=[bla ble blu]*180/pi;
bla=bla-noseoffset*pi/180;
qy=angle2quat(bla,0,0);
refquat=quatmultiply(quatinv(qy),refquat)
% [bla ble blu]=quat2angle(refquat);
% test=[bla ble blu]*180/pi
% newquat=quatmultiply(quatinv(qy),newquat);
% [yaw2 pitch2 roll2]=quat2angle(newquat);
% data2=[yaw2 pitch2 roll2]*180/pi

%refquat=[ -7545       -6448        7454 -10694]
%fid=fopen('test1.dat','w');
%ref=[19.1875   51.4375   -3.1250]
%ref=[0 0 0];
while ishandle(plotGraph3) %Loop when Plot is Active
    
    dat=fscanf(s,'%d');%Read yaw from Serial as Integer
 %   fprintf(fid,'%d %d %d %d %d %d %d %d %d %d %d \n',dat);
    if(~isempty(dat) & isnumeric(dat) & length(dat)==8) %Make sure yaw Type is Correct        
        count = count + 1;
        time(count) = toc;    %Extract Elapsed Time
        
% % with Euler angles
%         yaw(count)=dat(5)/16;
%         pitch(count)=dat(6)/16;
%         roll(count)=dat(7)/16;
%         
%         yawnotcorr(count)=yaw(count);
%         pitchnotcorr(count)=pitch(count);
%         rollnotcorr(count)=roll(count);
%         
%         yaw(count)=yaw(count)-ref(1);
%         pitch(count)=pitch(count)-ref(3);
%         roll(count)=roll(count)-ref(2);
%         yaw;
%         
%    % roll must remain between -180 and 180        
%         if roll(count)>180
%             roll(count)=roll(count)-360;
%         elseif roll(count)<-180
%             roll(count)=roll(count)+360;
%         end
% 
%    % pitch must remain between -90 and 90        
%         if pitch(count)>180
%             pitch(count)=pitch(count)-360;
%         end
%         
%    % heading must remain between 0 and 360
%         yaw(count) = mod(yaw(count),360);
%         if pitch(count)<min2
%             pitch(count) = pitch(count)+180;
%         end
%         if roll(count)<min3
%             roll(count) = roll(count)+360;
%         end
%         yaw(count);
%         pitch(count);
%         roll(count);


%with quaternions
        newquat=dat(1:4)';
        refquat=quatnormalize(refquat);
        newquat=quatnormalize(newquat);
        newquat=quatmultiply(newquat,quatinv(refquat));%le bon
        newquat=quatmultiply(quatinv(qy),newquat);
          
        [y p r]=quat2angle(newquat);
        yaw(count)=y*180/pi;
        if (yaw(count)<0)
            yaw(count)=yaw(count)+360;
        end
        roll(count)=p*180/pi;
        pitch(count)=r*180/pi;

% % with rotation quaternions
%         x0=[0 1 0 0];
%         newquat=dat(1:4)';
%         refquat=quatnormalize(refquat);
%         newquat=quatnormalize(newquat);
%         [y p r]=quat2angle(newquat);
%         testbef=[y p r]*180/pi
% 
%         newquat=quatmultiply(quatinv(qy),newquat);
%         [y p r]=quat2angle(newquat);
%         testaft=[y p r]*180/pi
%         
%         qn=quatmultiply(newquat,quatinv(refquat));
%         x1=quatmultiply(qn,quatmultiply(x0,quatinv(qn)));
%         pitch(count)=asin(x1(4))*180/pi;
%         yaw(count)=atan2(x1(3),x1(2))*180/pi;


        if(scrollWidth > 0)
        set(plotGraph,'Xdata',time(time > time(count)-scrollWidth),'Ydata',yaw(time > time(count)-scrollWidth));
%        axis([time(count)-scrollWidth time(count) min max]);

        set(plotGraph2,'Xdata',time(time > time(count)-scrollWidth),'Ydata',pitch(time > time(count)-scrollWidth));
%        axis([time(count)-scrollWidth time(count) min2 max2]);
        %plot(time(time > time(count)-scrollWidth),roll(time > time(count)-scrollWidth));
        set(plotGraph3,'Xdata',time(time > time(count)-scrollWidth),'Ydata',roll(time > time(count)-scrollWidth));
% %         axis([time(count)-scrollWidth time(count) min3 max3]);
%         %set(plotGraph,'Xyaw',time(time > time(count)-scrollWidth),'Ydata',roll(time > time(count)-scrollWidth));
%         %axis([time(count)-scrollWidth time(count) min max]);
        else
        set(plotGraph,'Xdata',time,'Ydata',yaw);
        axis([0 time(count) min max]);
        set(plotGraph2,'Xdata',time,'Ydata',pitch);
        axis([0 time(count) min2 max2]);
%         set(plotGraph3,'Xdata',time,'Ydata',roll);
%         axis([0 time(count) min3 max3]);
        end
        pause(delay)
        
        
        if (count>countMax)% & count>lastdetection+countMax)  %make sure that we have enough yaw to start the loops, and that we don't check again too early (avoid several recognitions of same motion)
        if (lastdetection>count-countMax+1)
            countStart=lastdetection+1;
        else
            countStart=count-countMax+1;  %analysing the frame between countstart and count (length countmax)
        end
            i=countStart;
            found=0;                    %used to exit the while loops when motion detected
            %%% TILTS %%%           
            while (i<=count-2 && found==0) 
                j=i+1;
                while (j<=count-1 && found==0)
                    if pitch(j)>pitch(i)+critAngle2 %pitch angle at least at critical angle (sufficient tilt)
                        k=j+1;
                        while (k<=count && found==0)
                            if (pitch(k)>pitch(i)-eps && pitch(k)<pitch(i)+eps) %pitch angle back to almost 0
                                lastdetection=k; %last time a recognized motion was detected
                                count
                                disp('ELBEE HERE!')
                                found=1;
                            end
                            k=k+1;
                        end
                    end
                    j=j+1;
                end
                %%% END TILT_DOWN%%
                
                j=i+1;
                while(j<=count-1&& found==0)
                    if pitch(j)<pitch(i)-critAngle2
                        k=j+1;
                        while(k<=count && found==0)
                            if (pitch(k)>pitch(i)-eps && pitch(k)<pitch(i)+eps)
                                lastdetection=k;
%                                clc
                                count
                                disp('_______________')
                                disp('TILT DOWN')
                                disp('_______________')
                                found=1;
                            end
                            k=k+1;
                        end
                    end
                    j=j+1;
                end
                
                %%% END TILT_UP%%
                j=i+1;
                while (j<=count-1 && found==0)
                    if roll(j)>roll(i)+critAngle3 %roll angle at least at critical angle 2(sufficient tilt)
                        k=j+1;
                        while (k<=count && found==0)
                            if (roll(k)>roll(i)-eps && roll(k)<roll(i)+eps) %roll angle back to almost 0
                                lastdetection=k; %last time a recognized motion was detected
                                count
                                disp('TILT_RIGHT!')
                                found=1;
                            end
                            k=k+1;
                        end
                    end
                    j=j+1;
                end
                %%% END TILT_RIGHT%%
                j=i+1;
                while(j<=count-1 && found==0)
                    if roll(j)<roll(i)-critAngle3 %roll angle lower than crit angle
                        k=j+1;
                        while (k<=count && found==0)
                            if (roll(k)<roll(i)+eps && roll(k)>roll(i)-eps)
                                lastdetection=k;
                                count
                                disp('TILT_LEFT!')
                                found=1;
                            end
                            k=k+1;
                        end
                    end
                    j=j+1;
                end
                
                %%% END TILT_LEFT%%
% START OF DETECTION OF ROTATION MOTIONS.
                j=i+1;
                if j>lastdetection
                    while (j<=count-1 && found==0)
                        if (mod(yaw(j)-yaw(i),360)>critAngle && mod(yaw(j)-yaw(i),360)<180) %change in heading angle larger than critical angle3, but not over 180 (impossible for human being, if the angle is larger it's an artefact of the mod function)
                            k=j+1;
                            while (k<=count && found==0)
                                if (yaw(i)>=360-eps)%e.g. yaw(i)=358, yaw(j)=18
                                    if (yaw(k)>yaw(i)-eps | yaw(k)<mod(yaw(i)+eps,360)) %heading angle back to the beginning
                                        lastdetection=k; %last time a recognized motion was detected
                                        count
                                        disp('ROTATE_LEFT1!')
                                        found=1;
                                    end
                                elseif (yaw(i)<=eps)%e.g. yaw(i)=1, yaw(j)=21
                                    if (yaw(k)<yaw(i)+eps | yaw(k)>mod(yaw(i)-eps,360)) %heading angle back to the beginning
                                        lastdetection=k; %last time a recognized motion was detected
                                        count
                                        disp('ROTATE_LEFT2!')
                                        found=1;
                                    end
                                else %e.g. yaw(i)=20, yaw(j)=40
                                    if (yaw(k)>yaw(i)-eps && yaw(k)<yaw(i)+eps) %heading angle back to the beginning
                                        lastdetection=k; %last time a recognized motion was detected
                                        count
                                        disp('ROTATE_LEFT3!')
                                        found=1;
                                    end
                                end
                                k=k+1;
                            end
                            %%% END ROTATE_LEFT%
                        elseif (mod(yaw(j)-yaw(i),360)>180 && mod(yaw(j)-yaw(i),360)<360-critAngle) %corresponds to a negative change in heading angle larger than critical angle3
                            k=j+1;
                            while (k<=count && found==0)
                                if (yaw(i)>=360-eps)%e.g. yaw(i)=358, yaw(j)=338
                                    if (yaw(k)>yaw(i)-eps | yaw(k)<mod(yaw(i)+eps,360)) %heading angle back to the beginning
                                        lastdetection=k; %last time a recognized motion was detected
                                        count
                                        disp('ROTATE_RIGHT1!')
                                        found=1;
                                    end
                                elseif (yaw(i)<=eps)%e.g. yaw(i)=1, yaw(j)=341
                                    if (yaw(k)<yaw(i)+eps | yaw(k)>mod(yaw(i)-eps,360)) %heading angle back to the beginning
                                        lastdetection=k; %last time a recognized motion was detected
                                        count
                                        disp('ROTATE_RIGHT2!')
                                        found=1;
                                    end
                                else %e.g. yaw(i)=65, yaw(j)=45
                                    if (yaw(k)>yaw(i)-eps && yaw(k)<yaw(i)+eps) %heading angle back to the beginning
                                        lastdetection=k; %last time a recognized motion was detected
                                        count
                                        disp('ROTATE_RIGHT3!')
                                        found=1;
                                    end
                                end
                                k=k+1;
                            end
                        end
                        j=j+1;
                    end
                end
                %%% END ROTATE_RIGHT%%
                i=i+1;
            end
        end
    end
end
                
        %Allow MATLAB to Update Plot
%        refreshyaw(plotGraph) % Evaluate y in the function workspace
%        drawnow; 
%        pause(delay)
%        refreshyaw(plotGraph2) % Evaluate y in the function workspace
%        drawnow; 
%        pause(delay)
%        refreshyaw(plotGraph3) % Evaluate y in the function workspace
%        drawnow; 
        

%Close Serial COM Port and Delete useless Variables
%fclose(fid);
fclose(s);
clear count dat delay max min baudRate plotGraph plotGrid plotTitle s ...
        scrollWidth serialPort xLabel yLabel;


disp('Session Terminated...');
clear


% FIRST TILT_UP, START AND GO BACK AROUND 0
%                 elseif (pitch(i-1)>-eps && pitch(i)<-eps)
%                     j=i+1;
%                     while(j<=count-1 && found==0)
%                         if pitch(j)<-critAngle %pitch angle lower than crit angle
%                             k=j+1;
%                             while (k<=count && found==0)
%                                 if (pitch(k-1)<-eps && pitch(k)>-eps)
%                                     lastdetection=k;
%                                     clc
%                                     count
%                                     disp('*********************************')
%                                     disp('ELBEE HERE!')
%                                     disp('*********************************')
%                                     found=1;
%                                 end
%                                 k=k+1;
%                             end
%                         end
%                         j=j+1;
%                     end
