clear %clear variables
clc %clear command window (clear screen)

%User Defined Properties 
serialPort = 'COM3';            % define COM port #
baudRate = 9600;                
freq = 10;                      %frequency of new yaw from IMU per sec
critTimeMin = 0.5;              %min time in which the movement must be done
critTimeMax = 2;                %max time in which the movement must be done
countMax=round(critTimeMax*freq);%maximum size of the frame used to check for specific head motion 
eps = 3;                        %difference of angle tolerated with respect to the initial position
critAngle  = 20;                %critical angle (rotate left or right) from which an action is triggered
critAngle2 = 15;                %critical angle (up or down) from which an action is triggered
critAngle3 = 15;                %critical angle (tilt left or right) from which an action is triggered
noseoffset = 30;                %offset of the direction of the nose with respect to the x-axis of the IMU 

time = 0;
yaw = 0;
pitch = 0;
roll = 0;
count = 0;

%Open Serial COM Port

tic %Start clock to measure performance

lastdetection=0; %initializing when is the last time we detected a recognized motion

disp('Press enter when ready for the reference');
pause;
s = serial(serialPort, 'BaudRate',baudRate);
disp('Close Plot to End Session');
fopen(s);
dat=fscanf(s,'%d');%Read data from Serial as Integer array
while (~isnumeric(dat)| isempty(dat)| length(dat)~=8)
    dat=fscanf(s,'%d');%Read data from Serial as Integer array
end
%get reference quaternion
refquat=[dat(1) dat(2) dat(3) dat(4)]
[offsetimu dummy1 dummy2]=quat2angle(refquat);
test0=[offsetimu dummy1 dummy2]*180/pi;
%the total offset is the sum of the IMU offset and the nose offset
offsetimu=offsetimu-noseoffset*pi/180;
qy=angle2quat(offsetimu,0,0);
refquat=quatmultiply(quatinv(qy),refquat)

while 1 %Loop when Plot is Active
    dat=fscanf(s,'%d');%Read yaw from Serial as Integer
    if(~isempty(dat) & isnumeric(dat) & length(dat)==8) %Make sure yaw Type is Correct        
        count = count + 1;
        time(count) = toc;    %Extract Elapsed Time
        
        newquat=dat(1:4)';
        %Normalize quaternions
        refquat=quatnormalize(refquat);
        newquat=quatnormalize(newquat); 
        %compute quaternion between 2 quaternions
        newquat=quatmultiply(newquat,quatinv(refquat));
        %perform same transformation as for quatref
        newquat=quatmultiply(quatinv(qy),newquat);
          
        [y p r]=quat2angle(newquat);
        yaw(count)=y*180/pi;
        if (yaw(count)<0)
            yaw(count)=yaw(count)+360;
        end
         roll(count)=p*180/pi;
        pitch(count)=r*180/pi;
        
        if (count>countMax)
        if (lastdetection>count-countMax+1)
            countStart=lastdetection+1;
        else
            countStart=count-countMax+1;  %analysing the frame between countstart and count (length countmax)
        end
            i=countStart;
            found=0;                    %used to exit the while loops when motion detected
            %%% TILTS %%%           
            while (i<=count-2 && found==0) 
                j=i+1;
                while (j<=count-1 && found==0)
                    if pitch(j)>pitch(i)+critAngle2 %pitch angle at least at critical angle (sufficient tilt)
                        k=j+1;
                        while (k<=count && found==0)
                            if (pitch(k)>pitch(i)-eps && pitch(k)<pitch(i)+eps) %pitch angle back to almost 0
                                lastdetection=k; %last time a recognized motion was detected
                                count
                                disp('ELBEE HERE!')
                                found=1;
                            end
                            k=k+1;
                        end
                    end
                    j=j+1;
                end
                %%% END TILT_DOWN%%
                
                j=i+1;
                while(j<=count-1&& found==0)
                    if pitch(j)<pitch(i)-critAngle2
                        k=j+1;
                        while(k<=count && found==0)
                            if (pitch(k)>pitch(i)-eps && pitch(k)<pitch(i)+eps)
                                lastdetection=k;
%                                clc
                                count
                                disp('_______________')
                                disp('TILT DOWN')
                                disp('_______________')
                                found=1;
                            end
                            k=k+1;
                        end
                    end
                    j=j+1;
                end
                
                %%% END TILT_UP%%
                j=i+1;
                while (j<=count-1 && found==0)
                    if roll(j)>roll(i)+critAngle3 %roll angle at least at critical angle 2(sufficient tilt)
                        k=j+1;
                        while (k<=count && found==0)
                            if (roll(k)>roll(i)-eps && roll(k)<roll(i)+eps) %roll angle back to almost 0
                                lastdetection=k; %last time a recognized motion was detected
                                count
                                disp('TILT_RIGHT!')
                                found=1;
                            end
                            k=k+1;
                        end
                    end
                    j=j+1;
                end
                %%% END TILT_RIGHT%%
                j=i+1;
                while(j<=count-1 && found==0)
                    if roll(j)<roll(i)-critAngle3 %roll angle lower than crit angle
                        k=j+1;
                        while (k<=count && found==0)
                            if (roll(k)<roll(i)+eps && roll(k)>roll(i)-eps)
                                lastdetection=k;
                                count
                                disp('TILT_LEFT!')
                                found=1;
                            end
                            k=k+1;
                        end
                    end
                    j=j+1;
                end
                
                %%% END TILT_LEFT%%
% START OF DETECTION OF ROTATION MOTIONS.
                j=i+1;
                if j>lastdetection
                    while (j<=count-1 && found==0)
                        if (mod(yaw(j)-yaw(i),360)>critAngle && mod(yaw(j)-yaw(i),360)<180) %change in heading angle larger than critical angle3, but not over 180 (impossible for human being, if the angle is larger it's an artefact of the mod function)
                            k=j+1;
                            while (k<=count && found==0)
                                if (yaw(i)>=360-eps)%e.g. yaw(i)=358, yaw(j)=18
                                    if (yaw(k)>yaw(i)-eps | yaw(k)<mod(yaw(i)+eps,360)) %heading angle back to the beginning
                                        lastdetection=k; %last time a recognized motion was detected
                                        count
                                        disp('ROTATE_LEFT1!')
                                        found=1;
                                    end
                                elseif (yaw(i)<=eps)%e.g. yaw(i)=1, yaw(j)=21
                                    if (yaw(k)<yaw(i)+eps | yaw(k)>mod(yaw(i)-eps,360)) %heading angle back to the beginning
                                        lastdetection=k; %last time a recognized motion was detected
                                        count
                                        disp('ROTATE_LEFT2!')
                                        found=1;
                                    end
                                else %e.g. yaw(i)=20, yaw(j)=40
                                    if (yaw(k)>yaw(i)-eps && yaw(k)<yaw(i)+eps) %heading angle back to the beginning
                                        lastdetection=k; %last time a recognized motion was detected
                                        count
                                        disp('ROTATE_LEFT3!')
                                        found=1;
                                    end
                                end
                                k=k+1;
                            end
                            %%% END ROTATE_LEFT%
                        elseif (mod(yaw(j)-yaw(i),360)>180 && mod(yaw(j)-yaw(i),360)<360-critAngle) %corresponds to a negative change in heading angle larger than critical angle3
                            k=j+1;
                            while (k<=count && found==0)
                                if (yaw(i)>=360-eps)%e.g. yaw(i)=358, yaw(j)=338
                                    if (yaw(k)>yaw(i)-eps | yaw(k)<mod(yaw(i)+eps,360)) %heading angle back to the beginning
                                        lastdetection=k; %last time a recognized motion was detected
                                        count
                                        disp('ROTATE_RIGHT1!')
                                        found=1;
                                    end
                                elseif (yaw(i)<=eps)%e.g. yaw(i)=1, yaw(j)=341
                                    if (yaw(k)<yaw(i)+eps | yaw(k)>mod(yaw(i)-eps,360)) %heading angle back to the beginning
                                        lastdetection=k; %last time a recognized motion was detected
                                        count
                                        disp('ROTATE_RIGHT2!')
                                        found=1;
                                    end
                                else %e.g. yaw(i)=65, yaw(j)=45
                                    if (yaw(k)>yaw(i)-eps && yaw(k)<yaw(i)+eps) %heading angle back to the beginning
                                        lastdetection=k; %last time a recognized motion was detected
                                        count
                                        disp('ROTATE_RIGHT3!')
                                        found=1;
                                    end
                                end
                                k=k+1;
                            end
                        end
                        j=j+1;
                    end
                end
                %%% END ROTATE_RIGHT%%
                i=i+1;
            end
        end
    end
end
fclose(s);
clear count dat delay max min baudRate plotGraph plotGrid plotTitle s ...
        scrollWidth serialPort xLabel yLabel;


disp('Session Terminated...');
clear