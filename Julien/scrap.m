% myfile=fopen('gonuts.txt','w');
% fprintf(myfile,'%6d %6d %6d %6d %6d %6d %6d %6d %6d %6d %6d %6d \r\n',q');
% fclose(myfile);

% myfile=fopen('headleft90.txt','r');
% sizer=[12 66]
% r=fscanf(myfile,'%6d %6d %6d %6d %6d %6d %6d %6d %6d %6d %6d %6d \r\n',sizer)
% fclose(myfile);

%s=[1 2 3 2 1];

% filetest=fopen('heya.txt','r');
% s=fscanf(filetest,'%d');
% disp(s);
% fclose(filetest);

% t = 0:pi/20:2*pi;
% y = exp(sin(t));
% h = plot(t,y,'YDataSource','y');
% for k = 1:.1:10
%  y = exp(sin(t.*k));
%  refreshdata(h,'caller') % Evaluate y in the function workspace
%  drawnow; pause(.1)
% end
eps=3;
critAngle3=30;
h=[320 10 320 10 320 10 10 10];
d=size(h);
count=d(2);
countMax=d(2)-1;
lastdetection=0;
for i=1:count-2;
    found=0;
    j=i+1;
    if j>lastdetection
        while (j<=count-1 && found==0)
            if (mod(h(j)-h(i),360)>critAngle3 && mod(h(j)-h(i),360)<180) %change in heading angle larger than critical angle3, but not over 180 (impossible for human being, if the angle is larger it's an artefact of the mod function)
                k=j+1;
                while (k<=count && found==0)
                    if (h(k)<h(i)+eps && h(k)>h(i)-eps) %heading angle back to the beginning
                        lastdetection=k; %last time a recognized motion was detected
                        i,j,k
                        disp('ROTATE_RIGHT!')
                        found=1;
                    end
                    k=k+1;
                end
            elseif (mod(h(j)-h(i),360)>180 && mod(h(j)-h(i),360)<360-critAngle3) %corresponds to a negative change in heading angle larger than critical angle3
                k=j+1;
                while (k<=count && found==0)
                    if (h(k)>=h(i)) %heading angle back to the beginning
                        lastdetection=k;
                        i,j,k
                        disp('ROTATE_LEFT!')
                        found=1;
                    end
                    k=k+1;
                end
            end
            j=j+1;
        end
    end
end
% if count>countMax
%     countStart=count-countMax;
%     i=1;
%     while (i<=count-2 && found==0) 
%         if (h(i)<eps & h(i)>-eps)
%             j=i+1;
%             while (j<=count-1 && found==0)
%                 if h(j)>critAngle
%                     k=j+1;
%                     while (k<=count & found==0)
%                         if (h(k)<eps & h(k)>-eps)
%                             disp('Elbee operational!')
%                             found=1;
%                         end
%                         k=k+1;
%                     end
%                 end
%                 j=j+1;
%             end
%         end
%         i=i+1;
%     end
% end
% countMax=20;
% 
% h=[1 1 2 3 4 5 6 7 8 9 10 10 10 9 8 6 5 0 4 3 0 0 11 11 0 0];
% d=size(h);
% count=d(2);
% countMax=d(2)-1;
% found=0;
% lastdetection=0;
% 
% if (count>countMax & count-countMax>lastdetection)  %make sure that we have enough data to start the loops, and that we don't check again too early (avoid several recognitions of same motion
%     countStart=count-countMax;  %analysing the frame between countstart and count (length countmax)
%     i=countStart;
%     found=0;                    %used to exit the while loops when motion detected
%     %%% TILT_RIGHT %%%
%     while (i<=count-2 && found==0)
%         if (data2(i)<eps && data2(i)>-eps) %pitch angle almost at 0
%             j=i+1;
%             while (j<=count-1 && found==0)
%                 if data2(j)>critAngle %pitch angle at least at critical angle (sufficient tilt)
%                     k=j+1;
%                     while (k<=count && found==0)
%                         if (data2(k)<eps && data2(k)>-eps) %pitch angle back to almost 0
%                             lastdetection=k; %last time a recognized motion was detected
%                             count
%                             disp('TILT_RIGHT!')
%                             found=1;
%                         end
%                         k=k+1;
%                     end
%                 end
%                 j=j+1;
%             end
%         end
%         i=i+1;
%     end
%     %%% END TILT_RIGHT%%
% end
