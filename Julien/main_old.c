#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <windows.h>

float quatnorm(int qin[4]) {
    int i;
    float norm = 0.0;
    for (i = 0; i < 4; i++) {
        norm = norm + qin[i] * qin[i];
    }
    norm = sqrt(norm);
    return norm;
}

void quatnormalize(int qin[4], float qout[4]) {
    int i;
    float norm;
    norm = quatnorm(qin);
    for (i = 0; i < 4; i++)
        qout[i] = qin[i] / norm;
}

void quatconj(float q[4]) {
    int i;
    for (i = 1; i < 4; i++)
        q[i] = -q[i];
}

void quatmult(float q1[4], float q2[4], float qout[4]) {
    qout[0] = (q1[0] * q2[0] - q1[1] * q2[1] - q1[2] * q2[2] - q1[3] * q2[3]);
    qout[1] = (q1[0] * q2[1] + q1[1] * q2[0] + q1[2] * q2[3] - q1[3] * q2[2]);
    qout[2] = (q1[0] * q2[2] - q1[1] * q2[3] + q1[2] * q2[0] + q1[3] * q2[1]);
    qout[3] = (q1[0] * q2[3] + q1[1] * q2[2] - q1[2] * q2[1] + q1[3] * q2[0]);
}

int main() {
    int i, j, k, t1[59], t2;
    int q0[59], q1[59], q2[59], q3[59], x1[4], intyaw[59], intpitch[59], introll[59];
    float qnormed1[4], qnormed2[4], qconj[4], qmult[4];
    float theta;
    float critangle = 20.0, critAngle2 = 20.0, eps = 3.0;
    double yaw[59], pitch[59], roll[59];
    int count = 0, countMax = 20, lastdetection = 0, countStart, found;
    
    printf("1 \r\n");
    
    FILE *ifile;
    HANDLE hSerial;
    hSerial = CreateFile("COM4",
            GENERIC_READ,
            0,
            0,
            OPEN_EXISTING,
            FILE_ATTRIBUTE_NORMAL,
            0);
    if (hSerial == INVALID_HANDLE_VALUE) {
        if (GetLastError() == ERROR_FILE_NOT_FOUND) {
            //serial port does not exist. Inform user.
                            printf("GetLastError error ");

        }
        //some other error occurred. Inform user.
                                    printf("GetLastError other error ");

    }
    printf("2 \r\n");
    DCB dcbSerialParams = {0};
    memset(&dcbSerialParams,0,sizeof(dcbSerialParams));
    dcbSerialParams.DCBlength = sizeof (dcbSerialParams);
    if (!GetCommState(hSerial, &dcbSerialParams)) {
        //error getting state
                printf("GetCommState error ");

    }
    // Common settings
dcbSerialParams.fOutxCtsFlow = FALSE;
dcbSerialParams.fOutxDsrFlow = FALSE;
dcbSerialParams.fDtrControl = DTR_CONTROL_DISABLE;
dcbSerialParams.fDtrControl = FALSE;
dcbSerialParams.fDsrSensitivity = FALSE;
dcbSerialParams.fOutX = FALSE;
dcbSerialParams.fInX = FALSE;
dcbSerialParams.fNull = FALSE;
dcbSerialParams.fRtsControl = RTS_CONTROL_DISABLE;
dcbSerialParams.fAbortOnError = FALSE;

    dcbSerialParams.BaudRate = 9600;
    dcbSerialParams.ByteSize = 8;
    dcbSerialParams.StopBits = ONESTOPBIT;
    dcbSerialParams.Parity = NOPARITY;
    if (!SetCommState(hSerial, &dcbSerialParams)) {
        //error setting serial port state    
        printf("SetCommState error ");

    }
    COMMTIMEOUTS timeouts = {0};
    timeouts.ReadIntervalTimeout = 1;
    timeouts.ReadTotalTimeoutConstant = 1;
    timeouts.ReadTotalTimeoutMultiplier = 1;
    timeouts.WriteTotalTimeoutConstant = 1;
    timeouts.WriteTotalTimeoutMultiplier = 1;
    if (!SetCommTimeouts(hSerial, &timeouts)) {
        //error occureed. Inform user
        printf("SetCommTimeouts error ");
    }
    printf("4 \r\n");
    int n=10;
    char szBuff[2] = {0};
    DWORD dwBytesRead = 0;
    if(!ReadFile(hSerial, szBuff, 2, &dwBytesRead, NULL)){
    //error occurred. Report to user.
                printf("ReadFile error ");

    }
    printf("read = %d %s\n",dwBytesRead, szBuff[0]);
    CloseHandle(hSerial);    

    
    ifile = fopen("arriere90.dat", "r");
    for (count = 0; count < 59; count++) //to replace by the real time data feed
    {
        fscanf(ifile, "%d%d%d%d%d%d%d%d\n", &q0[count], &q1[count], &q2[count], &q3[count], &intyaw[count], &intpitch[count], &introll[count], &t1[count]);
        yaw[count] = (double) intyaw[count] / 16;
        pitch[count] = (double) intpitch[count] / 16;
        roll[count] = (double) introll[count] / 16;
        //    	printf("%f %f %f\n",yaw[i],pitch[i],roll[i]);
        //roll must remain between -180 and 180
        if (roll[count] > 180)
            roll[count] = roll[count] - 360;
        else if (roll[count]<-180)
            roll[count] = roll[count] + 360;
        //heading must remain between 0 and 360
        if (yaw[count] > 360)
            yaw[count] = yaw[count] - 360;
        if (yaw[count] < 0)
            yaw[count] = yaw[count] + 360;

        //        printf("%d",count);
        if (count > countMax & count - countMax > lastdetection) //make sure that we have enough data to start the loops, and that we don't check again too early (avoid several recognitions of same motion
        {
            countStart = count - countMax + 1; //analysing the frame between countstart and count (length countmax)
            i = countStart;
            found = 0; //used to exit the while loops when motion detected
            //TILTS            
            while (i <= count - 2 && found == 0) {
                j = i + 1;
                while (j <= count - 1 && found == 0) {
                    //                    printf("%d %d %d\n",j,count,countStart);
                    if (pitch[j] > pitch[i] + critAngle2) //pitch angle at least at critical angle (sufficient tilt)
                    {
                        k = j + 1;
                        while (k <= count && found == 0) {
                            if (pitch[k] > pitch[i] - eps && pitch[k] < pitch[i] + eps) //pitch angle back to almost 0
                            {
                                lastdetection = k; //last time a recognized motion was detected
                                printf("%d", count);
                                printf("TILT_DOWN!");
                                found = 1;
                            }
                            k = k + 1;
                        }
                    }
                    j = j + 1;
                }
                i = i + 1;
            }

        }


    }

    //printf("%f %f %f %f %f\n",q1f[0],q1f[1],q1f[2],q1f[3],quatnorm(q1));
    fclose(ifile);
    // quatnormalize(q1,qnormed1);
    // quatnormalize(q2,qnormed2);
    // quatconj(qnormed2);
    // printf("%f %f %f %f\n",qnormed1[0],qnormed1[1],qnormed1[2],qnormed1[3]);
    // printf("%f %f %f %f\n",qnormed2[0],qnormed2[1],qnormed2[2],qnormed2[3]);
    // quatmult(qnormed1,qnormed2,qmult);
    // printf("%f %f %f %f\n",qmult[0],qmult[1],qmult[2],qmult[3]);
    // theta=2.*acos(qmult[0]);
    // printf("angle entre les 2 quaternions: %f degres",theta*180./M_PI);
    return 0;


}
