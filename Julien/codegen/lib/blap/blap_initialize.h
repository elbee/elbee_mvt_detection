/*
 * File: blap_initialize.h
 *
 * MATLAB Coder version            : 2.8
 * C/C++ source code generated on  : 28-Jul-2015 22:23:26
 */

#ifndef __BLAP_INITIALIZE_H__
#define __BLAP_INITIALIZE_H__

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "blap_types.h"

/* Function Declarations */
extern void blap_initialize(void);

#endif

/*
 * File trailer for blap_initialize.h
 *
 * [EOF]
 */
