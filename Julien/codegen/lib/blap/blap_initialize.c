/*
 * File: blap_initialize.c
 *
 * MATLAB Coder version            : 2.8
 * C/C++ source code generated on  : 28-Jul-2015 22:23:26
 */

/* Include Files */
#include "rt_nonfinite.h"
#include "blap.h"
#include "blap_initialize.h"

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : void
 */
void blap_initialize(void)
{
  rt_InitInfAndNaN(8U);
}

/*
 * File trailer for blap_initialize.c
 *
 * [EOF]
 */
