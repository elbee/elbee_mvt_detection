/*
 * File: blap.c
 *
 * MATLAB Coder version            : 2.8
 * C/C++ source code generated on  : 28-Jul-2015 22:23:26
 */

/* Include Files */
#include "rt_nonfinite.h"
#include "blap.h"

/* Function Definitions */

/*
 * Arguments    : double y[3]
 * Return Type  : void
 */
void blap(double y[3])
{
  int i0;
  for (i0 = 0; i0 < 3; i0++) {
    y[i0] = 1.0 + (double)i0;
  }
}

/*
 * File trailer for blap.c
 *
 * [EOF]
 */
