/*
 * File: blap_terminate.h
 *
 * MATLAB Coder version            : 2.8
 * C/C++ source code generated on  : 28-Jul-2015 22:23:26
 */

#ifndef __BLAP_TERMINATE_H__
#define __BLAP_TERMINATE_H__

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "blap_types.h"

/* Function Declarations */
extern void blap_terminate(void);

#endif

/*
 * File trailer for blap_terminate.h
 *
 * [EOF]
 */
