/*
 * File: blap_terminate.c
 *
 * MATLAB Coder version            : 2.8
 * C/C++ source code generated on  : 28-Jul-2015 22:23:26
 */

/* Include Files */
#include "rt_nonfinite.h"
#include "blap.h"
#include "blap_terminate.h"

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : void
 */
void blap_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for blap_terminate.c
 *
 * [EOF]
 */
