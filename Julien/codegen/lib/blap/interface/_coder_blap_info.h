/* 
 * File: _coder_blap_info.h 
 *  
 * MATLAB Coder version            : 2.8 
 * C/C++ source code generated on  : 28-Jul-2015 22:23:26 
 */

#ifndef ___CODER_BLAP_INFO_H__
#define ___CODER_BLAP_INFO_H__
/* Include Files */ 
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"


/* Function Declarations */ 
extern const mxArray *emlrtMexFcnResolvedFunctionsInfo(void);

#endif
/* 
 * File trailer for _coder_blap_info.h 
 *  
 * [EOF] 
 */
