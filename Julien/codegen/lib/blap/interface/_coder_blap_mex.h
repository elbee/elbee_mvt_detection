/*
 * File: _coder_blap_mex.h
 *
 * MATLAB Coder version            : 2.8
 * C/C++ source code generated on  : 28-Jul-2015 22:23:26
 */

#ifndef ___CODER_BLAP_MEX_H__
#define ___CODER_BLAP_MEX_H__

/* Include Files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "_coder_blap_api.h"

/* Function Declarations */
extern void mexFunction(int32_T nlhs, mxArray *plhs[], int32_T nrhs, const
  mxArray *prhs[]);

#endif

/*
 * File trailer for _coder_blap_mex.h
 *
 * [EOF]
 */
