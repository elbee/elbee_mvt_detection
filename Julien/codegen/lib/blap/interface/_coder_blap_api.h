/*
 * File: _coder_blap_api.h
 *
 * MATLAB Coder version            : 2.8
 * C/C++ source code generated on  : 28-Jul-2015 22:23:26
 */

#ifndef ___CODER_BLAP_API_H__
#define ___CODER_BLAP_API_H__

/* Include Files */
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include <stddef.h>
#include <stdlib.h>
#include "_coder_blap_api.h"

/* Variable Declarations */
extern emlrtCTX emlrtRootTLSGlobal;
extern emlrtContext emlrtContextGlobal;

/* Function Declarations */
extern void blap(real_T y[3]);
extern void blap_api(const mxArray *plhs[1]);
extern void blap_atexit(void);
extern void blap_initialize(void);
extern void blap_terminate(void);
extern void blap_xil_terminate(void);

#endif

/*
 * File trailer for _coder_blap_api.h
 *
 * [EOF]
 */
