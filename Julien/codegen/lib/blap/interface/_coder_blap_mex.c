/*
 * File: _coder_blap_mex.c
 *
 * MATLAB Coder version            : 2.8
 * C/C++ source code generated on  : 28-Jul-2015 22:23:26
 */

/* Include Files */
#include "_coder_blap_api.h"
#include "_coder_blap_mex.h"

/* Function Declarations */
static void blap_mexFunction(int32_T nlhs, mxArray *plhs[1], int32_T nrhs);

/* Function Definitions */

/*
 * Arguments    : int32_T nlhs
 *                const mxArray *plhs[1]
 *                int32_T nrhs
 * Return Type  : void
 */
static void blap_mexFunction(int32_T nlhs, mxArray *plhs[1], int32_T nrhs)
{
  const mxArray *outputs[1];
  int32_T b_nlhs;
  emlrtStack st = { NULL, NULL, NULL };

  st.tls = emlrtRootTLSGlobal;

  /* Check for proper number of arguments. */
  if (nrhs != 0) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:WrongNumberOfInputs", 5, 12, 0, 4, 4,
                        "blap");
  }

  if (nlhs > 1) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:TooManyOutputArguments", 3, 4, 4,
                        "blap");
  }

  /* Call the function. */
  blap_api(outputs);

  /* Copy over outputs to the caller. */
  if (nlhs < 1) {
    b_nlhs = 1;
  } else {
    b_nlhs = nlhs;
  }

  emlrtReturnArrays(b_nlhs, plhs, outputs);

  /* Module termination. */
  blap_terminate();
}

/*
 * Arguments    : int32_T nlhs
 *                const mxArray * const plhs[]
 *                int32_T nrhs
 *                const mxArray * const prhs[]
 * Return Type  : void
 */
void mexFunction(int32_T nlhs, mxArray *plhs[], int32_T nrhs, const mxArray
                 *prhs[])
{
  (void)prhs;

  /* Initialize the memory manager. */
  mexAtExit(blap_atexit);

  /* Module initialization. */
  blap_initialize();

  /* Dispatch the entry-point. */
  blap_mexFunction(nlhs, plhs, nrhs);
}

/*
 * File trailer for _coder_blap_mex.c
 *
 * [EOF]
 */
