clear %clear variables
%clear command window (clear screen)

%User Defined Properties 
serialPort = 'COM4';            % define COM port #
baudRate = 9600;                

%Open Serial COM Port
s = serial(serialPort, 'BaudRate',baudRate);
fopen(s);
myfile=fopen('rotdroite90.dat','w');
for i=1:60
    dat=fscanf(s,'%d');%Read Data from Serial as Integer
    i=0;
    length(dat);
    while (~isnumeric(dat)| isempty(dat)| length(dat)~=8)
        dat=fscanf(s,'%d');%Read Data from Serial as Integer
    end
    dat(5:7)/16
    fprintf(myfile,'%d %d %d %d %d %d %d %d \n',dat);
    %datread=fscanf(myfile,'%d %d %d %d %d %d %d %d %d %d %d \n');
%    datread
end
'done'
fclose(myfile);
% type avant90.dat
% myfile=fopen('test1.dat','r');
% for i=1:10
%     datread=fscanf(myfile,'%d',8 )
% end
% fclose(myfile);    
fclose(s);