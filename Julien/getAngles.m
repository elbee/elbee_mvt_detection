clear

f1='avant90.dat';
f2='arriere90.dat';
f3='rotdroite90.dat';
f4='rotgauche90.dat';
f5='droit90.dat';
f6='gauche90.dat';
myfile=fopen(f1,'r');
for i=1:1
    q=fscanf(myfile,'%d',8 )
end
ref=q(1:4,1)';
ref=quaternion(ref);
ref=ref.normalize
refang=real(ref);
refvect=vector(ref);
% x-axis of ref frame in ref frame
x=quaternion([0 1 0 0]);
% x-axis of ref frame in inertial frame
xref=product(inverse(ref),product(x,ref));
xrefvect=vector(xref);
xrefproj=[xrefvect(1) xrefvect(2) 0];
headref=atan2(xrefproj(2),xrefproj(1))
headrefbis=atan2(2*(refang*refvect(1)+refvect(2)*refvect(3)),1-2*(refvect(1)^2+refvect(2)^2))
xrefproj=xrefproj/norm(xrefproj);
pitchref=asin(xrefproj(3))
pitchrefbis=asin(2*refang*refvect(2)-refvect(3)*refvect(1))
% xrefprojz=[0 0 xref(4)];
% xrefprojz=xrefprojz/norm(xrefprojz);

y=quaternion([0 0 1 0]);
yref=product(inverse(ref),product(y,ref));
yrefvect=vector(yref);
yrefproj=[yrefvect(1) yrefvect(2) 0];
yrefproj=yrefproj/norm(yrefproj);


for i=1:59
    q=fscanf(myfile,'%d',8 );
    quat=q(1:4)';
    quat=quaternion(quat);
    quat=quat.normalize;
    x=quaternion([0 1 0 0]);
    xnew=product(inverse(quat),product(x,quat));
    xnewvect=vector(xnew);
    xnewproj=[xnewvect(1) xnewvect(2) 0];
    headnew=atan2(xnewvect(2),xnewvect(1));
    xnewproj=xnewproj/norm(xnewproj);
    head(i)=acos(dot(xnewproj,xrefproj));
    head(i)=head(i)*180/pi;
    headbis(i)=headnew-headref;
    headbis(i)=headbis(i)*180/pi;
%     if headbis(i)<0
%         headbis(i)=headbis(i)+360;
%     end
%     xnewprojz=[0 0 xnew(4)]
%     xnewprojz=xnewprojz/norm(xnewprojz);
    pitchnew=asin(xnewvect(3));
    pitch(i)=pitchnew-pitchref;
    pitch(i)=pitch(i)*180/pi;

    
%     xe=product(inverse(ref),product(x,ref))
%    x=product(inverse(ref),product(quat,product(x,product(inverse(quat),ref))))
%     y=[0 0 1 0];
% %     ye=product(inverse(ref),product(y,ref))
%     y=product(inverse(ref),product(quat,product(y,product(inverse(quat),ref))))
%     z=[0 0 0 1];
% %     ze=product(inverse(ref),product(z,ref))
%     z=product(inverse(ref),product(quat,product(z,product(inverse(quat),ref))))
end
head-headbis;
scrsz = get(0,'ScreenSize');
figure('Position',[0 0 scrsz(3)/2 scrsz(4)/2]);
plot(pitch)
title('Pitch','FontSize',20);
figure('Position',[scrsz(3)/2 0 scrsz(3)/2 scrsz(4)/2]);
plot(headbis)
title('Heading','FontSize',20);
% % figure
% % plot(pitch)
% 

fclose(myfile);    

% sizer=[12 66]
% q=fscanf(f,'%6d %6d %6d %6d %6d %6d %6d %6d %6d %6d %6d %6d \r\n',sizer)
