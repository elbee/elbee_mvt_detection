#include <Cstdio.h>
#include <stdlib.h>
#include <math.h>

float quatnorm(int qin[4]) {
    int i;
    float norm = 0.0;
    for (i = 0; i < 4; i++) {
        norm = norm + qin[i] * qin[i];
    }
    norm = sqrt(norm);
    return norm;
}

void quatnormalize(int qin[4], float qout[4]) {
    int i;
    float norm;
    norm = quatnorm(qin);
    for (i = 0; i < 4; i++)
        qout[i] = qin[i] / norm;
}

void quatconj(float q[4]) {
    int i;
    for (i = 1; i < 4; i++)
        q[i] = -q[i];
}

void quatmult(float q1[4], float q2[4], float qout[4]) {
    qout[0] = (q1[0] * q2[0] - q1[1] * q2[1] - q1[2] * q2[2] - q1[3] * q2[3]);
    qout[1] = (q1[0] * q2[1] + q1[1] * q2[0] + q1[2] * q2[3] - q1[3] * q2[2]);
    qout[2] = (q1[0] * q2[2] - q1[1] * q2[3] + q1[2] * q2[0] + q1[3] * q2[1]);
    qout[3] = (q1[0] * q2[3] + q1[1] * q2[2] - q1[2] * q2[1] + q1[3] * q2[0]);
}

int main() {
    int i, j, k, t1[59], t2;
    int q0[59], q1[59], q2[59], q3[59], x1[4], intyaw[59], intpitch[59], introll[59];
    float qnormed1[4], qnormed2[4], qconj[4], qmult[4];
    float theta;
    float critangle = 20.0, critAngle2 = 20.0, eps = 3.0;
    double yaw[59], pitch[59], roll[59];
    int count = 0, countMax = 20, lastdetection = 0, countStart, found;
    
    //SIGNAL TO WAKE UP IMU
    //.....
    
    //Open serial COM port
    //......
    
    
    //0. USER GO TO GET THE REFERENCE
    //.....
    
    //1. GET THE REFERENCE
    //Loop until we read a line of 8 integers
    dat=fscanf(s,'%d'); //Read Data from Serial as Integer
    
    //rotate the reference frame for the x-axis to point in the nose direction
    refquat=[dat(1) dat(2) dat(3) dat(4)];
    [bla ble blu]=quat2angle(refquat);
    bla=bla-noseoffset*pi/180;
    qy=angle2quat(bla,0,0);
    refquat=quatmultiply(quatinv(qy),refquat);
    
    while (imu_active)//Loop when Plot is Active
    
    { 
        dat=fscanf(s,'%d');//Read yaw from Serial as Integer
        {   
            count = count + 1;
            time(count) = toc;    //Extract Elapsed Time

            newquat=dat(1:4);
            refquat=quatnormalize(refquat);
            newquat=quatnormalize(newquat);
            newquat=quatmultiply(newquat,quatinv(refquat));
            newquat=quatmultiply(quatinv(qy),newquat);

            [y p r]=quat2angle(newquat);
            yaw(count)=y*180/pi;
            if (yaw(count)<0)
            {
                yaw(count)=yaw(count)+360;
            }
            roll(count)=p*180/pi;
            pitch(count)=r*180/pi;        

            if (count>countMax)% & count>lastdetection+countMax)  //make sure that we have enough yaw to start the loops, and that we don't check again too early (avoid several recognitions of same motion)
            {   
                if (lastdetection>count-countMax+1)
                {    
                    countStart=lastdetection+1;
                }
                else
                {
                    countStart=count-countMax+1;  //analysing the frame between countstart and count (length countmax)
                }

                i=countStart;
                found=0;                    //used to exit the while loops when motion detected
                // TILTS           
                while (i<=count-2 && found==0)
                {
                    j=i+1;
                    while (j<=count-1 && found==0)
                    {    
                        if pitch(j)>pitch(i)+critAngle2 //pitch angle at least at critical angle (sufficient tilt)
                        {
                            k=j+1;
                            while (k<=count && found==0)
                            {
                                if (pitch(k)>pitch(i)-eps && pitch(k)<pitch(i)+eps) //pitch angle back to almost 0
                                {
                                    lastdetection=k; //last time a recognized motion was detected
                                    count
                                    disp('ELBEE HERE!')
                                    found=1;
                                }
                                k=k+1;
                            }
                        }
                        j=j+1;
                    }
                    // END TILT_DOWN

                    j=i+1;
                    while(j<=count-1&& found==0)
                    {
                        if (pitch(j)<pitch(i)-critAngle2)
                        {
                            k=j+1;
                            while(k<=count && found==0)
                            {
                                if (pitch(k)>pitch(i)-eps && pitch(k)<pitch(i)+eps)
                                {
                                    lastdetection=k;
                                    count
                                    disp('_______________')
                                    disp('TILT DOWN')
                                    disp('_______________')
                                    found=1;
                                }
                                k=k+1;
                            }
                        }
                        j=j+1;
                    }

                    // END TILT_UP
                    j=i+1;
                    while (j<=count-1 && found==0)
                    {
                        if roll(j)>roll(i)+critAngle3 //roll angle at least at critical angle 2(sufficient tilt)
                        {
                            k=j+1;
                            while (k<=count && found==0)
                            {
                                if (roll(k)>roll(i)-eps && roll(k)<roll(i)+eps) //roll angle back to almost 0
                                {
                                    lastdetection=k; //last time a recognized motion was detected
                                    count
                                    disp('TILT_RIGHT!')
                                    found=1;
                                }
                                k=k+1;
                            }
                        }
                        j=j+1;
                    }
                    // END TILT_RIGHT
                    j=i+1;
                    while(j<=count-1 && found==0)
                    {
                        if roll(j)<roll(i)-critAngle3 //roll angle lower than crit angle
                        {
                            k=j+1;
                            while (k<=count && found==0)
                            {
                                if (roll(k)<roll(i)+eps && roll(k)>roll(i)-eps)
                                {
                                    lastdetection=k;
                                    count
                                    disp('TILT_LEFT!')
                                    found=1;
                                }
                                k=k+1;
                            }
                        }
                        j=j+1;
                    }

                    // END TILT_LEFT
                    // START OF DETECTION OF ROTATION MOTIONS.
                    j=i+1;
                    if (j>lastdetection)
                    {
                        while (j<=count-1 && found==0)
                        {
                            if (mod(yaw(j)-yaw(i),360)>critAngle && mod(yaw(j)-yaw(i),360)<180) //change in heading angle larger than critical angle3, but not over 180 (impossible for human being, if the angle is larger it's an artefact of the mod function)
                            {
                                k=j+1;
                                while (k<=count && found==0)
                                {
                                    if (yaw(i)>=360-eps)//e.g. yaw(i)=358, yaw(j)=18
                                    {
                                        if (yaw(k)>yaw(i)-eps | yaw(k)<mod(yaw(i)+eps,360)) //heading angle back to the beginning
                                        {
                                            lastdetection=k; //last time a recognized motion was detected
                                            count
                                            disp('ROTATE_LEFT1!')
                                            found=1;
                                        }
                                    }
                                    else
                                    {
                                        if (yaw(i)<=eps)//e.g. yaw(i)=1, yaw(j)=21
                                        {
                                            if (yaw(k)<yaw(i)+eps | yaw(k)>mod(yaw(i)-eps,360)) //heading angle back to the beginning
                                            {
                                                lastdetection=k; %last time a recognized motion was detected
                                                count
                                                disp('ROTATE_LEFT2!')
                                                found=1;
                                            }
                                        }
                                        else //e.g. yaw(i)=20, yaw(j)=40
                                        {
                                            if (yaw(k)>yaw(i)-eps && yaw(k)<yaw(i)+eps) //heading angle back to the beginning
                                            {
                                                lastdetection=k; %last time a recognized motion was detected
                                                count
                                                disp('ROTATE_LEFT3!')
                                                found=1;
                                            }
                                        }
                                    }
                                    k=k+1;
                                }
                            }
                                //END ROTATE_LEFT
                            else
                            {
                                if (mod(yaw(j)-yaw(i),360)>180 && mod(yaw(j)-yaw(i),360)<360-critAngle) //corresponds to a negative change in heading angle larger than critical angle3
                                {
                                    k=j+1;                             
                                    while (k<=count && found==0)
                                    {
                                        if (yaw(i)>=360-eps) //e.g. yaw(i)=358, yaw(j)=338
                                        {
                                            if (yaw(k)>yaw(i)-eps | yaw(k)<mod(yaw(i)+eps,360)) //heading angle back to the beginning
                                            {
                                                lastdetection=k; //last time a recognized motion was detected
                                                count
                                                disp('ROTATE_RIGHT1!')
                                                found=1;
                                            }
                                        }
                                        else
                                        {    
                                            if (yaw(i)<=eps)//e.g. yaw(i)=1, yaw(j)=341
                                            {    
                                                if (yaw(k)<yaw(i)+eps | yaw(k)>mod(yaw(i)-eps,360)) //heading angle back to the beginning
                                                {
                                                    lastdetection=k; //last time a recognized motion was detected
                                                    count
                                                    disp('ROTATE_RIGHT2!')
                                                    found=1;
                                                }
                                            }

                                            else //e.g. yaw(i)=65, yaw(j)=45
                                            {    
                                                if (yaw(k)>yaw(i)-eps && yaw(k)<yaw(i)+eps) //heading angle back to the beginning
                                                {
                                                    lastdetection=k; //last time a recognized motion was detected
                                                    count
                                                    disp('ROTATE_RIGHT3!')
                                                    found=1;
                                                }
                                            }
                                        k=k+1;
                                        }
                                    }
                                }
                            }
                            j=j+1;
                        }
                    }
                    // END ROTATE_RIGHT
                    i=i+1;
                }
            }
        }
    }



    
    for (count = 0; count < 59; count++) //to replace by the real time data feed
    {
        fscanf(ifile, "%d%d%d%d%d%d%d%d\n", &q0[count], &q1[count], &q2[count], &q3[count], &intyaw[count], &intpitch[count], &introll[count], &t1[count]);
        yaw[count] = (double) intyaw[count] / 16;
        pitch[count] = (double) intpitch[count] / 16;
        roll[count] = (double) introll[count] / 16;
        //    	printf("%f %f %f\n",yaw[i],pitch[i],roll[i]);
        //roll must remain between -180 and 180
        if (roll[count] > 180)
            roll[count] = roll[count] - 360;
        else if (roll[count]<-180)
            roll[count] = roll[count] + 360;
        //heading must remain between 0 and 360
        if (yaw[count] > 360)
            yaw[count] = yaw[count] - 360;
        if (yaw[count] < 0)
            yaw[count] = yaw[count] + 360;

        //        printf("%d",count);
        if (count > countMax & count - countMax > lastdetection) //make sure that we have enough data to start the loops, and that we don't check again too early (avoid several recognitions of same motion
        {
            countStart = count - countMax + 1; //analysing the frame between countstart and count (length countmax)
            i = countStart;
            found = 0; //used to exit the while loops when motion detected
            //TILTS            
            while (i <= count - 2 && found == 0) {
                j = i + 1;
                while (j <= count - 1 && found == 0) {
                    //                    printf("%d %d %d\n",j,count,countStart);
                    if (pitch[j] > pitch[i] + critAngle2) //pitch angle at least at critical angle (sufficient tilt)
                    {
                        k = j + 1;
                        while (k <= count && found == 0) {
                            if (pitch[k] > pitch[i] - eps && pitch[k] < pitch[i] + eps) //pitch angle back to almost 0
                            {
                                lastdetection = k; //last time a recognized motion was detected
                                printf("%d", count);
                                printf("TILT_DOWN!");
                                found = 1;
                            }
                            k = k + 1;
                        }
                    }
                    j = j + 1;
                }
                i = i + 1;
            }

        }


    }

    //printf("%f %f %f %f %f\n",q1f[0],q1f[1],q1f[2],q1f[3],quatnorm(q1));
    fclose(ifile);
    // quatnormalize(q1,qnormed1);
    // quatnormalize(q2,qnormed2);
    // quatconj(qnormed2);
    // printf("%f %f %f %f\n",qnormed1[0],qnormed1[1],qnormed1[2],qnormed1[3]);
    // printf("%f %f %f %f\n",qnormed2[0],qnormed2[1],qnormed2[2],qnormed2[3]);
    // quatmult(qnormed1,qnormed2,qmult);
    // printf("%f %f %f %f\n",qmult[0],qmult[1],qmult[2],qmult[3]);
    // theta=2.*acos(qmult[0]);
    // printf("angle entre les 2 quaternions: %f degres",theta*180./M_PI);
    return 0;


}
