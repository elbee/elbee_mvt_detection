clear %clear variables
clc %clear command window (clear screen)

%User Defined Properties 
serialPort = 'COM5';            % define COM port #
baudRate = 9600;                
plotTitle = 'Heading Data Log'; % plot title
xLabel = 'Time (s)';            % x-axis label
yLabel = 'Heading';             % y-axis label
plotGrid = 'on';                % 'off' to turn off grid
min = 0;                        % set y-min for heading
max = 360;                      % set y-max
min2= -90;                      % set y-min for pitch
max2= 90;                       % set y-min 
min3= -180;                     % set y-min for roll
max3= 180;                      % set y-max 
scrollWidth = 10;               % display period in plot, plot entire data log if <= 0
delay = .01;                    % make sure sample faster than resolution
freq = 10;                      %frequency of new data from IMU per sec
critTimeMin = 0.5;              %min time in which the movement must be done
critTimeMax = 2;                %max time in which the movement must be done
countMax=round(critTimeMax*freq);%maximum size of the frame used to check for specific head motion 
eps = 3;                        %difference of angle tolerated with respect to the initial position
critAngle = 22;                 %critical angle (up or down) from which an action is triggered
critAngle2 = 30;                %critical angle (tilt left or right) from which an action is triggered
critAngle3 = 30;                %critical angle (rotate left or right) from which an action is triggered
critAngle = 20;                 %critical angle (up or down) from which an action is triggered
critAngle2 = 20;                %critical angle (tilt left or right) from which an action is triggered
critAngle3 = 20;                %critical angle (rotate left or right) from which an action is triggered
%Define reference: the reference frame along the head axes in the IMU
%reference frame (will be given by a function called at calibration)
ref=[ 74.8750   -8.5000 -125.3750];
%ref=[0 0 0];
refquat=[4918 13293 -4703 -6740];
ref=[0 0 0];
refquat=[   13436        8717  2086       -2750];

%Define Function Variables
time = 0;
count = 0;
data = 0;
data2 = 0;
data3 = 0;

%Set up Plot
%Heading
scrsz = get(0,'ScreenSize');
figure('Position',[0 2*scrsz(4)/3 2*scrsz(3)/3 scrsz(4)/4.3]);
plotGraph = plot(time,data);%,time,data2,time,data3);
title(plotTitle,'FontSize',20);
xlabel(xLabel,'FontSize',10);
ylabel(yLabel,'FontSize',10);
%axis([0 10 min max]);

grid(plotGrid);
%Pitch
figure('Position',[0 scrsz(4)/3 2*scrsz(3)/3 scrsz(4)/4.3]);
plotGraph2 = plot(time,data2);%,time,data2,time,data3);
title('Pitch Data Log','FontSize',20);
xlabel(xLabel,'FontSize',10);
ylabel('Pitch','FontSize',10);
%axis([0 10 min2 max2]);

grid(plotGrid);
%Roll
figure('Position',[0 0 2*scrsz(3)/3 scrsz(4)/4.3]);
plotGraph3 = plot(time,data3);%,time,data2,time,data3);
title('Roll Data Log','FontSize',20);
xlabel(xLabel,'FontSize',10);
grid(plotGrid);

%Open Serial COM Port
s = serial(serialPort, 'BaudRate',baudRate);
disp('Close Plot to End Session');
fopen(s);

tic %Start clock to measure performance

lastdetection=0; %initializing when is the last time we detected a recognized motion
 
quit

while ishandle(plotGraph3) %Loop when Plot is Active
     
    dat=fscanf(s,'%d');%Read Data from Serial as Integer
    if(~isempty(dat) & isnumeric(dat) & length(dat)==11) %Make sure Data Type is Correct        
        count = count + 1;
        time(count) = toc;    %Extract Elapsed Time

         data(count) = dat(5)/16-ref(1); %for heading
         data2(count) = dat(6)/16-ref(2);
         data3(count) = dat(7)/16-ref(3);
%         dat(1:4)=dat(1:4)-refquat;
%         
%         [y p r]=quat2angle(dat(1:4)');
%         data(count)=y*180/pi;
%         data2(count)=p*180/pi;
%         data3(count)=r*180/pi;
        
%         newquat=dat(1:4)';
%         refquat=quatnormalize(refquat);
%         newquat=quatnormalize(newquat);
%         newquat=quatmultiply(newquat,quatinv(refquat));
%         
%         [y p r]=quat2angle(newquat);
%         data(count)=-y*180/pi;
%         data2(count)=-r*180/pi;
%         data3(count)=p*180/pi;
        if (scrollWidth>0)
        set(plotGraph,'XData',time(time > time(count)-scrollWidth),'YData',data(time > time(count)-scrollWidth));
%        axis([time(count)-scrollWidth time(count) min max]);

        set(plotGraph2,'XData',time(time > time(count)-scrollWidth),'YData',data2(time > time(count)-scrollWidth));
%        axis([time(count)-scrollWidth time(count) min2 max2]);
        %plot(time(time > time(count)-scrollWidth),data3(time > time(count)-scrollWidth));
        set(plotGraph3,'XData',time(time > time(count)-scrollWidth),'YData',data3(time > time(count)-scrollWidth));
%         axis([time(count)-scrollWidth time(count) min3 max3]);
        %set(plotGraph,'XData',time(time > time(count)-scrollWidth),'YData',data3(time > time(count)-scrollWidth));
        %axis([time(count)-scrollWidth time(count) min max]);
        else
            set(plotGraph,'XData',time,'YData',data);
            axis([0 time(count) min max]);
            set(plotGraph2,'XData',time,'YData',data2);
            axis([0 time(count) min2 max2]);
            set(plotGraph3,'XData',time,'YData',data3);
            axis([0 time(count) min3 max3]);
        end
        pause(delay)
    end
end
fclose(s)