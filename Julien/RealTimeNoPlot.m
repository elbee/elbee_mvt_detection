clear %clear variables
%clear command window (clear screen)

%User Defined Properties 
serialPort = 'COM3';            % define COM port #
baudRate = 9600;                

%Define Function Variables
time = 0;
data = 0;
data2 = 0;
data3 = 0;
count = 0;


%Open Serial COM Port
s = serial(serialPort, 'BaudRate',baudRate);
fopen(s);

tic %Start clock to measure performance
% ref=[289.4375  -24.0000 -152.5000];   
% refquat=[12848        9605        3296 -495];   
% dat=fscanf(s,'%d');%Read Data from Serial as Integer
% i=0;
% length(dat);
% prompt='Press enter when ready for the reference';
% input(prompt);
dat=fscanf(s,'%d');%Read Data from Serial as Integer
while (~isnumeric(dat)| isempty(dat)| length(dat)~=8)
    dat=fscanf(s,'%d');%Read Data from Serial as Integer
end

% ref=[dat(5)/16 dat(6)/16 dat(7)/16]
% refquat=[dat(1) dat(2) dat(3) dat(4)];
% disp('Press enter when ready for the reference');
% 
% pause
ref=[24.3822  -26.6843  -91.2225];
ref;
refquat=[14025 -7870 -1531 -2727];
[yaw pitch roll]=quat2angle(refquat);
refangle=[yaw pitch roll]*180/pi
dat=fscanf(s,'%d');%Read Data from Serial as Integer
while (~isnumeric(dat)| isempty(dat)| length(dat)~=8)
    disp('i"m in!!')
    dat=fscanf(s,'%d')%Read Data from Serial as Integer
end
x0=[0 1 0 0]

data(1)=dat(5)/16;
data(2)=dat(6)/16;
data(3)=dat(7)/16;
data;

newquat=dat(1:4)';
[bla ble blu]=quat2angle(newquat);
newangle=[bla ble blu]*180/pi
qn=quatmultiply(newquat,quatinv(refquat))
x1=quatmultiply(qn,quatmultiply(x0,quatinv(qn)))
pitch=asin(x1(4))*180/pi
heading=atan2(x1(3),x1(2))*180/pi

quatfinal=quatmultiply(newquat,quatinv(refquat))%le bon
[bla ble blu]=quat2angle(quatfinal);
anglefinal=[bla ble blu]*180/pi

% qy=angle2quat(yaw,0,0);
% newquat=quatmultiply(quatinv(qy),newquat);
% [yaw2 pitch2 roll2]=quat2angle(newquat);
% data2=[yaw2 pitch2 roll2]*180/pi
% 
% transrefquat=quatmultiply(quatinv(qy),refquat);
% transnewquat=quatmultiply(quatinv(qy),newquat);
% q01=quatmultiply(transnewquat,quatinv(transrefquat));
% [ke la kot]=quat2angle(q01);
% soooo=[ke la kot]*180/pi
% refquat=quatnormalize(refquat);
% newquat=quatnormalize(newquat);
% newquat=quatmultiply(newquat,quatinv(refquat));

% [y p r]=quat2angle(newquat);
% y=y*180/pi;
% p=p*180/pi;
% r=r*180/pi;

% quat=dat(1:4)'
% quat=quat/2^14
% quat=quatmultiply(quat,quatinv(quat))

% quat=quatnormalize(quat);
% [y p r]=quat2angle(quat,'XZX')
% y=y*180/pi;
% p=p*180/pi;
% r=r*180/pi;
% [y p r];
% 
% newdata=data-ref;

fclose(s);