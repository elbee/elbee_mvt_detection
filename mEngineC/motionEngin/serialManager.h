//
//  serialManager.h
//  motionEngin
//
//  Created by vincent canuel on 30/07/15.
//  Copyright © 2015 elbee. All rights reserved.
//

#ifndef serialManager_c
#define serialManager_c

#include <stdio.h>

int set_interface_attribs (int fd, int speed, int parity);

void set_blocking (int fd, int should_block);



#endif /* serialManager_c */
