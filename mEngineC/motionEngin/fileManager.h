//
//  fileManager.h
//  motionEngin
//
//  Created by vincent canuel on 14/08/15.
//  Copyright © 2015 elbee. All rights reserved.
//

#ifndef fileManager_c
#define fileManager_c

#include <stdio.h>

int initFile();

int closeFile();

int writeData(double yaw, double pitch, double roll, int time);

#endif /* fileManager_c */
