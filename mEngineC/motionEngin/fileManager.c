//
//  fileManager.c
//  motionEngin
//
//  Created by vincent canuel on 14/08/15.
//  Copyright © 2015 elbee. All rights reserved.
//

#include "fileManager.h"
#include<stdio.h>


FILE *ofp;
static char *outputFilename = "/Users/vcanuel/data.raw";

int initFile()
{
    ofp = fopen(outputFilename, "w+"); //open and erase
    
    if (ofp == NULL) {
        fprintf(stderr, "Can't open output file %s!\n",
                outputFilename);
        return -1;
    }
    
    return 0;
}

int closeFile()
{
    
    if (ofp == NULL) {
        fprintf(stderr, "Can't close output file: NULL %s!\n",
                outputFilename);
        return -1;
    }
    
    fclose(ofp);
    ofp = NULL;

    return 0;
}

int writeData(double yaw, double pitch, double roll, int time) {
    
   if (ofp == NULL) {
       fprintf(stderr, "Can't write to output file: NULL %s!\n",
               outputFilename);
       return -1;
   }
   else {
       fprintf(ofp, "%f %f %f %d \n", yaw, pitch, roll, time);
       fflush (ofp);    // flushing or repositioning required
   }
    
    return 1;
}