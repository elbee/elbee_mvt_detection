//
//  quaternion.c
//  motionEngin
//
//  Created by vincent canuel on 31/07/15.
//  Copyright © 2015 elbee. All rights reserved.
//

#include "quaternion.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

quaternion_t *quaternion_new(void)
{
    return malloc(sizeof(quaternion_t));
}

quaternion_t *quaternion_new_set(double q1,
                                 double q2,
                                 double q3,
                                 double q4)
{
    quaternion_t *q = malloc(sizeof(quaternion_t));
    if (q != NULL) {
        q->q[0] = q1; q->q[1] = q2; q->q[2] = q3; q->q[3] = q4;
    }
    return q;
}


int quaternion_copy(quaternion_t *r, quaternion_t *q)
{
    size_t i;
    
    if (r == NULL || q == NULL) {
        fprintf(stderr, "NULL quaternion_copy\n");
        return -1;
    }

    for(i = 0; i < 4; i++) r->q[i] = q->q[i];
    return 0;
}


int quaternion_normq(double *normq, quaternion_t *q)
{
    size_t i;
    
    if (q == NULL) {
        fprintf(stderr, "NULL quaternion in quaternion_normq\n");
        return -1;
    }
    
    for(i = 0; i < 4; i++) *normq+= q->q[i] * q->q[i];
    return 0;
}

int quaternion_norm(double *norm, quaternion_t *q)
{
    double r = 0;
    if (quaternion_normq(&r, q) == -1 ) {
        fprintf(stderr, "error in quaternion_norm \n");
        return -1;
    }
     *norm =sqrt(r);
    return 0;
}

int quaternion_normalize(quaternion_t *r, quaternion_t *q)
{
    double norm = 0;
    if (quaternion_norm(&norm, q) == -1) {
        fprintf(stderr, "NULL quaternion_normalize\n");
        return -1;
    }
    quaternion_div_d(r, q, norm);
    return 0;
}

int quaternion_inv(quaternion_t *inversequaternion, quaternion_t *quaternion)
{
    double normsq = 0.0;
    quaternion_t *conjquat  = quaternion_new();

    if (quaternion == NULL || inversequaternion == NULL) {
        fprintf(stderr, "NULL quaternion in quaternion_inv\n");
        free(conjquat);
        return -1;
    }
    
    quaternion_conj(conjquat,quaternion);
    quaternion_normq(&normsq, quaternion);
    
    if (normsq > 0.) {
        quaternion_div_d(inversequaternion, conjquat, normsq);
    } else{
        fprintf(stderr, "quaternion_inv normsq is <0 \n");
        free(conjquat);
        return -1;
    }
    
    free(conjquat);
    return 0;
}


int quaternion_neg(quaternion_t *r, quaternion_t *q)
{
    size_t i;
    
    if (q == NULL || r == NULL) {
        fprintf(stderr, "quaternion_neg NULL\n");
        return -1;
    }

    for(i = 0; i < 4; i++) r->q[i] = -q->q[i];
    return 0;
}


int quaternion_conj(quaternion_t *r, quaternion_t *q)
{
    size_t i;
    
    if (q == NULL || r == NULL) {
        fprintf(stderr, "quaternion_conj NULL\n");
        return -1;
    }

    r->q[0] = q->q[0];
    for(i = 1; i < 4; i++) r->q[i] = -q->q[i];
    return 0;
}


int quaternion_add_d(quaternion_t *r, quaternion_t *q, double d)
{
    if (q == NULL || r == NULL) {
        fprintf(stderr, "quaternion_add_d NULL\n");
        return -1;
    }
    
    quaternion_copy(r, q);
    r->q[0] += d;
    return 0;
}


int quaternion_add(quaternion_t *r, quaternion_t *a, quaternion_t *b)
{
    size_t i;
    
    if (r == NULL || a == NULL || b == NULL) {
        fprintf(stderr, "quaternion_add NULL\n");
        return -1;
    }

    for(i = 0; i < 4; i++) r->q[i] = a->q[i] + b->q[i];
    return 0;
}


int quaternion_mul_d(quaternion_t *r, quaternion_t *q, double d)
{
    size_t i;
    
    if (r == NULL || q == NULL) {
        fprintf(stderr, "quaternion_mul_d NULL\n");
        return -1;
    }

    for(i = 0; i < 4; i++) r->q[i] = q->q[i] * d;
    return 0;
}

int quaternion_div_d(quaternion_t *r, quaternion_t *q, double d)
{
    size_t i;
    
    if (r == NULL || q == NULL) {
        fprintf(stderr, "quaternion_div_d NULL\n");
        return -1;
    }

    for(i = 0; i < 4; i++) r->q[i] = q->q[i] / d;
    return 0;
}

bool quaternion_equal(quaternion_t *a, quaternion_t *b)
{
    size_t i;
    
    for(i = 0; i < 4; i++) if (a->q[i] != b->q[i]) return false;
    return true;
}


#define A(N) (a->q[(N)])
#define B(N) (b->q[(N)])
#define R(N) (r->q[(N)])
int quaternion_mul(quaternion_t *r, quaternion_t *a, quaternion_t *b)
{
    if (r == NULL || a == NULL || b == NULL) {
        fprintf(stderr, "quaternion_mul NULL\n");
        return -1;
    }

    R(0) = A(0)*B(0) - A(1)*B(1) - A(2)*B(2) - A(3)*B(3);
    R(1) = A(0)*B(1) + A(1)*B(0) + A(2)*B(3) - A(3)*B(2);
    R(2) = A(0)*B(2) - A(1)*B(3) + A(2)*B(0) + A(3)*B(1);
    R(3) = A(0)*B(3) + A(1)*B(2) - A(2)*B(1) + A(3)*B(0);
    return 0;
}
#undef A
#undef B
#undef R


int quaternion_print(quaternion_t *q)
{
    if (q == NULL){
        fprintf(stderr, "quaternion_print NULL\n");
        return -1;
    }

    printf("(%lf, %lf, %lf, %lf)\n", 
           q->q[0], q->q[1], q->q[2], q->q[3]);
    return 0;
}


/* quaternion_2_angles  -- Convert a Quaternion into Euler Angles 
-- q can be non-normalised quaternion
 */

int quaternion_2_angles (double * yaw, double * pitch, double * roll, quaternion_t *q) {
    
    if (!yaw|| !pitch || !roll || !q) {
        fprintf(stderr, "quaternion_2_angles NULL\n");
        return -1;
    }
    
    float sqw = (float)q->q[0]*q->q[0];
    double sqx = q->q[1]*q->q[1];
    double sqy = q->q[2]*q->q[2];
    double sqz = q->q[3]*q->q[3];
    double unit = sqx + sqy + sqz + sqw; // if normalised is one, otherwise is correction factor
    //double unit = 1.; //TODO check this with MATHLAB
    double test = q->q[1]*q->q[2] + q->q[3]*q->q[0];
    if (test > 0.499*unit) { // singularity at north pole
        *yaw = 2 * atan2(q->q[1],q->q[0]);
        *pitch = M_PI/2;
        *roll = 0;
        return 0;
    }
    if (test < -0.499*unit) { // singularity at south pole
        *yaw  = -2 * atan2(q->q[1],q->q[0]);
        *pitch = -M_PI/2;
        *roll  = 0;
        return 0;
    }
    
    // Order of applying rotations : Rx(Rz(Ry)))  (about y,z then x)
    *yaw  = atan2(2*q->q[2]*q->q[0]-2*q->q[1]*q->q[3] , sqx - sqy - sqz + sqw);
    *pitch  = asin(2*test/unit);
    *roll  = atan2(2*q->q[1]*q->q[0]-2*q->q[2]*q->q[3] , -sqx + sqy - sqz + sqw);
    
    //MATHLAB  Rx(Ry(Rz))) // TODO check with real mathlab
    *yaw  = asin(2*test/unit);
    *pitch  = atan2(2*q->q[2]*q->q[0]-2*q->q[1]*q->q[3] , sqx - sqy - sqz + sqw);
    *roll  = atan2(2*q->q[1]*q->q[0]-2*q->q[2]*q->q[3] , -sqx + sqy - sqz + sqw);
    
//    *yaw  = atan2(-2*(q->q[2]*q->q[3]-q->q[0]*q->q[1]) , sqw - sqx - sqy + sqz);
//    *pitch  = asin(2*(q->q[1]*q->q[3] +q->q[0]*q->q[2]));
//    *roll  = atan2(-2*(q->q[1]*q->q[2] -q->q[0]*q->q[3]) , 2*(q->q[1] * q->q[3] + q->q[0]* q->q[2]));

     return 0;
}

/* angles_2_quaternion -- Convert an Euler to Quaternion (angle are in radian) */

int angles_2_quaternion ( quaternion_t *rotquaternion, double yaw, double pitch, double roll) {
    double cx, cy, cz, sx, sy, sz;
    
    if (!rotquaternion) {
        fprintf(stderr, "angles_2_quaternion NULL\n");
        return -1;
    }
    
    cx = cos(yaw/2.);
    sx = sin(yaw/2.);
    
    cy = cos(pitch/2.);
    sy = sin(pitch/2.);
    
    cz = cos(roll/2.);
    sz = sin(roll/2.);
    
    // Order of applying rotations : Rx(Rz(Ry)))  (about y,z then x)
//    rotquaternion->q[0] = cx*cy*cz - sx*sy*sz;
//    rotquaternion->q[1] = cx*cy*sz + sx*sy*cz;
//    rotquaternion->q[2] = sx*cy*cz + cx*sy*sz;
//    rotquaternion->q[3] = cx*sy*cz - sx*cy*sz;
    
    //CQRLIB  Rz(Ry(Rx)))
//    rotquaternion->q[0] = cx*cy*cz + sx*sy*sz;
//    rotquaternion->q[1]  = sx*cy*cz - cx*sy*sz;
//    rotquaternion->q[2]  = cx*sy*cz + sx*cy*sz;
//    rotquaternion->q[3]  = cx*cy*sz - sx*sy*cz;
    
    //MATHLAB  Rx(Ry(Rz))) // TODO check with real mathlab
    rotquaternion->q[0] = cx*cy*cz + sx*sy*sz;
    rotquaternion->q[1]  = cx*cy*sz - sx*sy*cz;
    rotquaternion->q[2]  = cx*sy*cz + sx*cy*sz;
    rotquaternion->q[3]  = sx*cy*cz - cx*sy*sz;
     return 0;
}
