//
//  quaternion.h
//  motionEngin
//
//  Created by vincent canuel on 31/07/15.
//  Copyright © 2015 elbee. All rights reserved.
//

#ifndef quaternion_c
#define quaternion_c

#include <stdbool.h>



typedef struct quaternion
{
    double q[4];
} quaternion_t;

quaternion_t *quaternion_new(void);
quaternion_t *quaternion_new_set(double q1,
                                 double q2,
                                 double q3,
                                 double q4);
int quaternion_copy(quaternion_t *r, quaternion_t *q);
int quaternion_normq(double *normq, quaternion_t *q);
int quaternion_norm(double *norm, quaternion_t *q);
int quaternion_normalize(quaternion_t *r, quaternion_t *q);
int quaternion_neg(quaternion_t *r, quaternion_t *q);
int quaternion_conj(quaternion_t *r, quaternion_t *q);
int quaternion_inv(quaternion_t *inversequaternion, quaternion_t *quaternion);
int quaternion_add_d(quaternion_t *r, quaternion_t *q, double d);
int quaternion_add(quaternion_t *r, quaternion_t *a, quaternion_t *b);
int quaternion_mul_d(quaternion_t *r, quaternion_t *q, double d);
int quaternion_div_d(quaternion_t *r, quaternion_t *q, double d);
bool quaternion_equal(quaternion_t *a, quaternion_t *b);
int quaternion_mul(quaternion_t *r, quaternion_t *a, quaternion_t *b);
int quaternion_print(quaternion_t *q);
int quaternion_2_angles (double * RotX, double * RotY, double * RotZ, quaternion_t *rotquaternion);
int angles_2_quaternion (quaternion_t *rotquaternion, double RotX, double RotY, double RotZ);


#endif /* quaternion_c */
