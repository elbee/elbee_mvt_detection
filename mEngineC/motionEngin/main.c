//
//  main.c
//  motionEngin
//
//  Created by vincent canuel on 30/07/15.
//  Copyright © 2015 elbee. All rights reserved.
//
#include <stdlib.h>
#include <math.h>
#include <errno.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>

#include "serialManager.h"
#include "quaternion.h"
#include "fileManager.h"

/* GLOBAL STATIC VARIABLES */
static char *portname = "/dev/cu.usbmodemFA131";
static double mNoseoffset = 30 * M_PI/180;     //offset of the direction of the nose with respect to the x-axis of the IMU
static double mEps = 4 * M_PI/180;                        //difference of angle tolerated with respect to the initial position
static double mCritAnglePitch = 15 * M_PI/180;     //critical angle (up or down) from which an action is triggered
static double mCritAngleRoll = 15 * M_PI/180;     //critical angle (right or left) from which an action is triggered
static double mCritAngleYaw = 15 * M_PI/180;     //critical angle (rotate ) from which an action is triggered
static int mCountMax=20; //=TIME *freq maximum size of the frame used to check for specific head motion

/* GLOBAL VARIABLES */

enum movementEnum {
    noMovement = -1,
    pitchUp,
    pitchDown,
    rollLeft,
    rollRight,
    rotateRight,
    rotateLeft
};

typedef enum movementEnum MOVEMENT;    // create a typedef.

double mYaw[20], mPitch[20], mRoll[20], mCurStartMovementAngle;
int mCount, mLastCurtime, mCurStartMovementIndex;
MOVEMENT mCurStartMovement, mCurEndMovement;
bool mIsQuatRefValid = false;
quaternion_t *refquatInv, *refQuatYInv, *initQuat, *initQuatNorm,*initQuatMulInvRefQuat,*newquat;

//Internal functions
void initLoopMemory();
void resetLoopVariables();
bool updateRefQuat(quaternion_t *initQuat);
bool isMovementDetect();
bool detectEndMovement(double curPitch,  double curRoll,  double curYaw);
bool detectStartMovement(int i,  double curPitch,  double curRoll,  double curYaw);

void initLoopMemories() {
    initQuat  = quaternion_new();
    initQuatNorm  = quaternion_new();
    initQuatMulInvRefQuat = quaternion_new();
    newquat  = quaternion_new();
    refquatInv = quaternion_new();
    refQuatYInv = quaternion_new();
}

void resetLoopVariables() {
    memset(&mPitch[0], 0, sizeof(mPitch));
    memset(&mYaw[0], 0, sizeof(mYaw));
    memset(&mRoll[0], 0, sizeof(mRoll));
    
    mCount = 0;
    mLastCurtime=0;
    mCurStartMovement = noMovement;
    mCurEndMovement = noMovement;
    mCurStartMovementIndex=-1;
    mCurStartMovementAngle = -1;
}

bool updateRefQuat(quaternion_t *initQuat) {
    if (mIsQuatRefValid) {
        return true;
    }
    
    //Simulate wait by randomy wait for picking 10
    if ( (rand() % 50) != 10) {
        return false;
    }
    
    double yaw, pitch, roll;
    int res = 0;
    quaternion_t *refQuatY =quaternion_new();
    quaternion_t *refquat =quaternion_new();

    // Extract YAW, PITCH, ROLL
    
    res |= quaternion_2_angles(&yaw, &pitch, &roll, initQuat);
    
    // Compute refQuat : the total offset is the sum of the IMU offset and the nose offset
    yaw=yaw-mNoseoffset;
    res |= angles_2_quaternion(refQuatY, yaw, 0, 0);
    res |= quaternion_inv(refQuatYInv, refQuatY);
    res |= quaternion_mul(refquat, refQuatYInv, initQuat);

    // invalidate crazy values
    res |= (roll !=0 && pitch != 0 && roll != M_PI && pitch != M_PI) ? 0 : -1;
    //Save inverse of refQuat for future computation
    res |= quaternion_inv(refquatInv, refquat);
    
    printf("refquatInv = "); quaternion_print(refquatInv);

    
    if (res!=0) {
        printf("updateRefQuat : invalid \n");
        mIsQuatRefValid = false;
    }
    else {
        printf("updateRefQuat : [%f,%f,%f] \n", yaw*180/M_PI, pitch*180/M_PI, roll*180/M_PI);
        mIsQuatRefValid = true;
    }
    
    //Free memory
    free(refQuatY);
    free(refquat);

    return mIsQuatRefValid;
}

bool isMovementDetect() {
    //Find current angle for each axis
    double curPitch  = mPitch[mCount];
    double curRoll = mRoll[mCount];
    double curYaw  = mYaw[mCount];
    
    //Detect if movement back in progress
    if (detectEndMovement(curPitch, curRoll, curYaw)) {
        return true;
    }
    
    // Detection Frame = [curIndex -> 0] == T-1, T-2,..T-X
    for (int i=mCount-1; i>=0; i--) {
        if (detectStartMovement(i, curPitch, curRoll, curYaw)) {
            return true;
        }
    }
    
    // Detection Frame = [mCountMax -> curIndex] == T-X-1, T-X-2, etc..
    for (int i=mCountMax-1; i>mCount; i--) {
        if (detectStartMovement(i, curPitch, curRoll, curYaw)) {
            return true;
        }
    }
    
    return false;
}

bool detectEndMovement(double curPitch,  double curRoll,  double curYaw) {
    double curMovementAngle = -1;
    
    //Movement already detected
    if(mCurEndMovement != noMovement) {
        return true;
    }

    //Start Movement detected
    if (mCurStartMovement != noMovement) {
        
        curMovementAngle = (mCurStartMovement == pitchUp || mCurStartMovement == pitchDown) ? curPitch : curMovementAngle;
        curMovementAngle = (mCurStartMovement == rollLeft || mCurStartMovement == rollRight) ? curRoll : curMovementAngle;
        curMovementAngle = (mCurStartMovement == rotateLeft || mCurStartMovement == rotateRight) ? curYaw : curMovementAngle;
        
        //test if angle back to almost the initial Position
        if (curMovementAngle>mCurStartMovementAngle-mEps && curMovementAngle<mCurStartMovementAngle+mEps) {
            mCurEndMovement=mCurStartMovement;
            return true; // Movement start & end found
        }
        return true; // Only movement start found == valid
    }
        return false;
}


bool detectStartMovement(int i,  double curPitch,  double curRoll,  double curYaw) {
    //Already found
    if (mCurStartMovement != noMovement) {
        return true;
    }
    
    //Pitch UP
    if (mPitch[i]!=0. && mPitch[i]!=-0. && curPitch > mPitch[i]+mCritAnglePitch) {
        printf("PITCH UP START: %d %f <-> %d %f \n", mCount, curPitch*180/M_PI, i,mPitch[i]*180/M_PI);
        mCurStartMovement = pitchUp;
        mCurStartMovementIndex = i;
        mCurStartMovementAngle = mPitch[i];
        return true;
    }
    //Pitch down
    if (mPitch[i]!=0. && mPitch[i]!=-0. && curPitch < mPitch[i]-mCritAnglePitch) {
        printf("PITCH DOWN START: %d %f <-> %d %f \n", mCount, curPitch*180/M_PI, i,mPitch[i]*180/M_PI);
        mCurStartMovement = pitchDown;
        mCurStartMovementIndex = i;
        mCurStartMovementAngle = mPitch[i];
        return true;
    }
    
    //Roll Left
    if (mRoll[i]!=0. && mRoll[i]!=-0. && curRoll > mRoll[i]+mCritAngleRoll) {
        printf("ROLL LEFT START: %d %f <-> %d %f \n", mCount, curRoll*180/M_PI, i,mRoll[i]*180/M_PI);
        mCurStartMovement = rollLeft;
        mCurStartMovementIndex = i;
        mCurStartMovementAngle = mRoll[i];
        return true;
    }
    //Roll Right
    if (mRoll[i]!=0. && mRoll[i]!=-0. && curRoll < mRoll[i]-mCritAngleRoll) {
        printf("ROLL RIGHT START: %d %f <-> %d %f \n", mCount, curRoll*180/M_PI, i,mRoll[i]*180/M_PI);
        mCurStartMovement = rollRight;
        mCurStartMovementIndex = i;
        mCurStartMovementAngle = mRoll[i];
        return true;
    }
    //Rotate left
//    if (mYaw[i]!=0. && mYaw[i]!=-0. && curYaw > mYaw[i]+mCritAngleYaw) {
//        printf("ROTATE LEFT START: %d %f <-> %d %f \n", mCount, curYaw*180/M_PI, i,mYaw[i]*180/M_PI);
//        mCurStartMovement = rotateLeft;
//        mCurStartMovementIndex = i;
//        mCurStartMovementAngle = mYaw[i];
//        return true;
//    }
    //Rotate right
//    if (mYaw[i]!=0. && mYaw[i]!=-0. && curYaw < mYaw[i]-mCritAngleYaw) {
//        printf("ROTATE RIGHT START: %d %f <-> %d %f \n", mCount, curYaw*180/M_PI, i,mYaw[i]*180/M_PI);
//        mCurStartMovement = rotateRight;
//        mCurStartMovementIndex = i;
//        mCurStartMovementAngle = mYaw[i];
//        return true;
//    }
    return false;
}


void mainLoop(double q0, double q1,double q2,double q3, int curTime) {
    double yawRad = 0, pitchRad = 0, rollRad = 0;
    mLastCurtime = curTime;
    
    if (initQuat != NULL) {
        initQuat->q[0] = q0; initQuat->q[1] = q1; initQuat->q[2] = q2; initQuat->q[3] = q3;
    }
    
    //Update REF QUAT
    if (!updateRefQuat(initQuat)) {
        printf("ref is invalid wait for the next iteraion \n");
        return;
    }
    
    // Resest detection if movement was found OR restart frame OR Timeout
    if (curTime - mLastCurtime > 5000  || mCurEndMovement != noMovement   || mCount == mCurStartMovementIndex) {
        //printf("RESET %d %d %d %d \n", curTime, mPitchUpStartIndex, mPitchUpEndIndex, mCount);
        resetLoopVariables();
    }
 
    //  Normalize quaternions
    quaternion_normalize(initQuatNorm, initQuat);

    // compute quaternion between ref and current
    quaternion_mul(initQuatMulInvRefQuat, initQuatNorm, refquatInv);

    // compute quaternion between ref Y and previous compute
    quaternion_mul(newquat, refQuatYInv, initQuatMulInvRefQuat);

    
    // Extract YAW, PITCH, ROLL
    quaternion_2_angles(&yawRad, &pitchRad, &rollRad, newquat);
    mYaw[mCount] = (yawRad <0) ? yawRad + 2*M_PI : yawRad;
    mPitch[mCount] = pitchRad;
    mRoll[mCount] = rollRad;
    
    //DEBUG PRINT
    printf("T=%d Y=%f P=%f R=%f \n",mCount, mYaw[mCount]*180/M_PI, mPitch[mCount]*180/M_PI, mRoll[mCount]*180/M_PI);
    writeData(mYaw[mCount]*180/M_PI, mPitch[mCount]*180/M_PI, mRoll[mCount]*180/M_PI, curTime);
    
    //MOVEMENT DETECTION
    if (isMovementDetect()) {
        
        switch (mCurEndMovement) {
            case pitchUp:
                printf("*** ELBEE UP ***\n");
                break;
            case pitchDown:
                printf("*** ELBEE DOWN ***\n");
                break;
            case rollLeft:
                printf("*** ROLL LEFT ***\n");
                break;
            case rollRight:
                printf("*** ROLL RIGHT ***\n");
                break;
            case rotateLeft:
                printf("*** ROTATE LEFT ***\n");
                break;
            case rotateRight:
                printf("*** ROTATE RIGHT ***\n");
                break;
            default:
                printf("MVT in progress: C=%d SM=%d SI=%d SA=%f \n", mCount, mCurStartMovement, mCurStartMovementIndex, mCurStartMovementAngle*180/M_PI);
                break;
        }
        
    }
    
    // Increment lopp
    mCount = (mCount+1) % mCountMax;
}

int main(int argc, const char * argv[]) {
    printf("Start, World!\n");
    
    char buf [1];
    char line [50];
    int resultParse = 0;
    int quat1, quat2, quat3, quat4, curTime;
    int eulX, eulY, eulZ;

    initFile();
    initLoopMemories();
    resetLoopVariables();

    int fd = open(portname, O_RDONLY | O_NOCTTY | O_SYNC);
    if (fd < 0)
    {   
        fprintf (stderr, "error %d opening %s: %s \n", errno, portname, strerror (errno));
        return 1;
    }
    
    set_interface_attribs (fd, B9600, 0);  // set speed to 9600 bps, 8n1 (no parity)
    set_blocking (fd, 1);                // set blocking
    int count = 0;
    
    while (1) {
        read(fd, buf, sizeof buf);  // read up to 1 characters if ready to read
        line[count] = buf[0];
        count = count+1 % 50;
        //new line found
        if (buf[0] == '\n') {
            //Parse line
            resultParse = sscanf(line, "%d %d %d %d %d %d %d %d ", &quat1, &quat2, &quat3, &quat4,&eulX,&eulY,&eulZ, &curTime);
            // Check if line is valid
            if (isnumber(line[0]) && resultParse == 8) {
                mainLoop(quat1, quat2, quat3, quat4, curTime);
            }
            memset(line, '\0', 50 ); // reinit
            count = 0;
        }
    }
    
    return 0;
}
