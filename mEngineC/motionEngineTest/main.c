//
//  main.c
//  motionEngineTest
//
//  Created by vincent canuel on 10/08/15.
//  Copyright © 2015 elbee. All rights reserved.
//

#include <stdio.h>
#include <math.h>
#include "minunit.h"
#include "quaternion.h"

int tests_run = 0;
int noseoffset = 30;                //offset of the direction of the nose with respect to the x-axis of the IMU

static char * test_conv_quat_to_euler_null() {
    //GIVEN
    quaternion_t *initQuat  = quaternion_new_set(1., 1., 1., 1.);
    double yaw = 0, pitch = 0;
    //THEN
    quaternion_2_angles(&yaw, &pitch, NULL, initQuat);
    //THAT
    mu_assert("error, test_conv_quat_to_euler_null yaw != 0", yaw == 0);
    return 0;
}

static char * test_conv_quat_to_euler_nullQuat() {
    //GIVEN
    double yaw=0, pitch=0, roll=0;
    //THEN
    quaternion_2_angles(&yaw, &pitch, &roll, NULL);
    //THAT
    mu_assert("error, yaw != 0", yaw == 0);
    return 0;
}

static char * test_conv_quat_to_euler() {
    //GIVEN
    quaternion_t *initQuat  = quaternion_new_set(1., 0., 1., 0.);
    double yaw, pitch, roll;
    //THEN
    quaternion_2_angles(&yaw, &pitch, &roll, initQuat);
    //THAT
    mu_assert("error, yaw != 0", yaw == 0);
    mu_assert("error, pitch != 1.5707963267948966", pitch == 1.5707963267948966);
    mu_assert("error, roll != 0", roll == 0);

    return 0;
}

static char * test_conv_quat_to_euler2() {
    //GIVEN
    double rot90Rad = 0.5*M_PI;
    double cosRot = cos(rot90Rad/2.);
    quaternion_t *initQuat  = quaternion_new_set(cosRot, 0, 0, cosRot);
    //THEN
    double yaw, pitch, roll;
    quaternion_2_angles(&yaw, &pitch, &roll, initQuat);
    //THAT
    mu_assert("error, test_conv_quat_to_euler2 yaw != 90", pitch == rot90Rad);
    return 0;
}

static char * test_conv_quat_to_euler3() {
    //GIVEN
    double yaw = 0, pitch = 0, roll = 0;
    double yawDeg = 0, pitchDeg = 0, rollDeg = 0;
    quaternion_t *initQuat  = quaternion_new_set(-3.563, 1.005, 5.774, 7.116);
    //THEN
    quaternion_2_angles(&yaw, &pitch, &roll, initQuat);
    yawDeg = yaw * 180/M_PI;
    pitchDeg = pitch * 180/M_PI;
    rollDeg = roll * 180/M_PI;
    //THAT
    //mu_assert("error, test_conv_quat_to_euler2 pitch != 90", pitchDeg == 90);
    return 0;
}

static char * test_conv_euler_to_quat_null() {
    //GIVEN
    double yaw = 0, pitch= 0, roll = 0;
    //THEN
    angles_2_quaternion(NULL, yaw, pitch, roll);
    //THAT
    mu_assert("error, test_conv_euler_to_quat W != 1", yaw == 0);
    return 0;
}

static char * test_conv_euler_to_quat() {
    //GIVEN
    double yaw = 0, pitch= 0, roll = 0;
    double yawRad = yaw*M_PI/180, pitchRad= pitch*M_PI/180 , rollRad = roll*M_PI/180;
    //THEN
    quaternion_t *resultQuat  = quaternion_new();
    angles_2_quaternion(resultQuat, yawRad, pitchRad, rollRad);
    //THAT
    mu_assert("error, test_conv_euler_to_quat W != 1", resultQuat->q[0] == 1);
    return 0;
}

static char * test_conv_euler_to_quat2() {
    //GIVEN
    double yaw = 0, pitch= 90. , roll = 0;
    double yawRad = yaw*M_PI/180, pitchRad= pitch*M_PI/180 , rollRad = roll*M_PI/180;
    //THEN
    quaternion_t *resultQuat  = quaternion_new();
    angles_2_quaternion(resultQuat, yawRad, pitchRad, rollRad);
    //THAT
    mu_assert("error, test_conv_euler_to_quat W != .5", resultQuat->q[0] == cos(pitchRad/2.));
    return 0;
}

static char * test_conv_euler_to_quat3() {
    //GIVEN
    double yaw = 3.5564268871862876, pitch= 90. , roll = 0;
    double yawRad = yaw*M_PI/180, pitchRad= pitch*M_PI/180 , rollRad = roll*M_PI/180;
    //THEN
    quaternion_t *resultQuat  = quaternion_new();
    angles_2_quaternion(resultQuat, yawRad, pitchRad, rollRad);
    //THAT
    mu_assert("error, test_conv_euler_to_quat W != .5", resultQuat->q[0] == 0.706766261158828);
    return 0;
}

static char * test_conv_euler_to_quat4() {
    //GIVEN
    double yaw = 0.7854, pitch= 0.1 , roll = 0;
   // double yawDeg = yaw*180/M_PI, pitchDeg= pitch*180/M_PI, rollDeg = roll*180/M_PI;

    //THEN
    quaternion_t *resultQuat  = quaternion_new();
    angles_2_quaternion(resultQuat, yaw, pitch, roll);
    //THAT
    mu_assert("error, test_conv_euler_to_quat W != .5", resultQuat->q[0] == 0.92272457268933594);
    mu_assert("error, test_conv_euler_to_quat X != .5", resultQuat->q[1] ==	-0.019126242445565825);
    mu_assert("error, test_conv_euler_to_quat Y != .5", resultQuat->q[2] == 0.046174713977463394);
    mu_assert("error, test_conv_euler_to_quat Z != .5", resultQuat->q[3] ==	0.38220602506278639);

    return 0;
}

static char * all_tests() {
    mu_run_test(test_conv_quat_to_euler_null);
    mu_run_test(test_conv_quat_to_euler_nullQuat);
    mu_run_test(test_conv_euler_to_quat_null);    
    mu_run_test(test_conv_quat_to_euler);
    mu_run_test(test_conv_quat_to_euler2);
    mu_run_test(test_conv_quat_to_euler3);

    mu_run_test(test_conv_euler_to_quat);
    mu_run_test(test_conv_euler_to_quat2);
    mu_run_test(test_conv_euler_to_quat3);
    mu_run_test(test_conv_euler_to_quat4);


    return 0;
}

int main(int argc, char **argv) {
    char *result = all_tests();
    if (result != 0) {
        printf("%s\n", result);
    }
    else {
        printf("ALL TESTS PASSED\n");
    }
    printf("Tests run: %d\n", tests_run);
    
    return result != 0;
}
